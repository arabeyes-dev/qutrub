#!/usr/bin/python
# -*- coding=utf-8 -*-
# Copyright 2004 Gregory Trubetskoy <grisha@modpython.org>
#

#
#    A Mod_python Example: Using Publisher with PSP
#

"""

This example demonstrates a practical use of the Publisher
handler in conjunction with PSP as a templating mechanism.

Here we have a farily typical corporate site with a few
pages: "home", "product" and "about". The site contains
a menu which lets you go to any one of those pages. The
menu highlights the page you're on.

The layout of the site uses the main template, which
consists of an HTML table which provides space for the
menu component and the body component.

Apache config:

  SetHandler mod_python
  PythonHandler mod_python.publisher

"""

import os
import xml.sax.saxutils as saxutils
import datetime
from mod_python import psp
import sys
from config import *
sys.path.append("F:\Program Files\EasyPHP 2.0b1\www\python\qutrub/libqutrub/src");
from mosaref_main import *


# subdirectory where we place templates
TMPL_DIR = "templates"
# the name for the "shell" template
MAIN_TMPL = "main_frame.html"
# the menu template
MENU_TMPL = "main_menu.html"

##
# External functions. These can be accessed via URL's from
# outside.


def index(req):
    # The publisher will call this function as default,
    # make it same as home
    word = req.form.getfirst('verb', False)
    if not word:
        return home(req)
    word = word.decode('utf8')
    haraka = req.form.getfirst('haraka', FATHA)
    haraka = haraka.decode('utf8')
    root = req.form.getfirst('root', 'root')
    display_format = req.form.getfirst('display_format', 'HTML')

    colordiacritics = req.form.getfirst('colordiacritics', False)
    if colordiacritics=="on":
        colordiacritics=u"on"
        if display_format=="HTML":
            display_format=u"HTMLColoredDiacritics"
    else: colordiacritics=u"off"

    transitive = req.form.getfirst('transitive', False)
    if transitive:
        transitive = True

    all = req.form.getfirst('all', False)
    if all:
        all = True

    past = req.form.getfirst('past', False)
    if past:
        past = True

    future = req.form.getfirst('future', False)
    if future:
        future = True

    imperative = req.form.getfirst('imperative', False)
    if imperative:
        imperative = True

    passive = req.form.getfirst('passive', False)
    if passive:
        passive = True
    future_moode = req.form.getfirst('future_moode', False)
    if future_moode:
        future_moode = True
    confirmed = req.form.getfirst('confirmed', False)
    if confirmed:
        confirmed = True

    word = word.strip(' ')
    if is_valid_infinitive_verb(word):
# suggest is used to give more suggestion for triliteral verbs
        suggest="";
        if is_triliteral_verb(word):
# search the future haraka for the triliteral verb
            db_base_path=os.path.join(_base_directory(req),"libqutrub/");
            liste_verb=find_triliteral_verb(db_base_path,word,haraka);
# if there are more verb forms, select the first one
            if  len(liste_verb)>0:
                word=liste_verb[0]["verb"]
                haraka=liste_verb[0]["haraka"]
                transitive=liste_verb[0]["transitive"]
                if len(liste_verb)>1: suggest=u"هل تقصد؟<br/>"
# the other forms are suggested
                for i in range(1,len(liste_verb)):

                    suggested_word=liste_verb[i]["verb"]
                    suggested_haraka=liste_verb[i]["haraka"]
                    suggested_transitive=liste_verb[i]["transitive"]
                    future_form=get_future_form(suggested_word,suggested_haraka);
#                    display_format=display_format.decode("utf8");
                    suggest+=u"""<a href='?verb=%s&haraka=%s&transitive=%s&all=1&display_format=HTML&colordiacritics=%s'>%s %s</a><br/>"""%(suggested_word,suggested_haraka,suggested_transitive,colordiacritics,suggested_word,future_form);
# if the verb does'nt exist in the triliteral verb dictionary
#            else:suggest=u"غير وارد في المعجم<br/>"
            else:suggest=db_base_path


        result=do_sarf(word,haraka,all,past,future,passive,imperative,future_moode,confirmed,transitive,display_format)
# write givven verb into the verblog log
        writelog(req,word,haraka,all,past,future,passive,imperative,confirmed, future_moode,transitive)
        return conjugate(req,result,suggest)
    else:
##        word+="-Error"
# suggest is used to give more suggestion for triliteral verbs
        suggest="";
        # suggest a list of verb, uf the verb is invalid
    	liste_verb=suggest_verb(word);
        if len(liste_verb)>0: suggest=u"هل تقصد؟<br/>"
# the other forms are suggested
        for i in range(len(liste_verb)):

            suggested_word=liste_verb[i]#.decode("utf8")
            suggested_haraka=u"فتحة"
            suggested_transitive=True
##            future_form=get_future_form(suggested_word,suggested_haraka);
#                    display_format=display_format.decode("utf8");
            suggest+=u"""<a href='?verb=%s&haraka=%s&transitive=%s&all=1&display_format=HTML&colordiacritics=%s'>%s </a><br/>"""%(suggested_word,suggested_haraka,suggested_transitive,colordiacritics,suggested_word);
# if the verb does'nt exist in the triliteral verb dictionary

# write givven verb into the verblog log, with error message
    	writelog(req,word+"Error",haraka,all,past,future,passive,imperative,confirmed, future_moode,transitive)

    	return _error(req,saxutils.escape(word),suggest);

#------------------------------------------
# Insert all users input into a database for logging
#------------------------------------------
def writelog(req,word,haraka,all,past,future,passive,imperative,confirmed, future_moode,transitive):
    try:
        #connection
        db_path=os.path.join(_base_directory(req),"data/verblog.db")
        conn = sqlite.connect(db_path)

    ##    conn = sqlite3.connect('verblog.db')
        c = conn.cursor()
        #--------------------
        # query
        #--------------------
        timelog=datetime.datetime.now();
        verb=word;
        haraka=u'فتحة';
        haraka=haraka[0];
        transitive=True;
        if transitive:transitive=u'م'
        else: transitive=u'ل'
    ##    tenses=u'pfiP';
        if all:
        	tenses=u"الكل";
        else:
        	tenses=u"";
        	if past: tenses+=u"ي";
        	else: tenses+="-";
        	if future: tenses+=u"ع";
        	else: tenses+="-";
        	if imperative: tenses+=u"م";
        	else: tenses+="-";
        	if passive: tenses+=u"ل";
        	else: tenses+=u"-";
        	if future_moode: tenses+=u"ا";
        	else: tenses+=u"-";
        	if confirmed: tenses+=u"ن";
        	else: tenses+=u"-";
        unvocalised=ar_strip_marks_keepshadda(verb);
        t=(verb,haraka,transitive,tenses,timelog,unvocalised)
        # Insert a row of data
        c.execute("""insert into verblog
                  values (?,?,?,?,?,?)""",t)
        #commit
        conn.commit()
        #close
        c.close()
    except:
        return None;
def _base_directory(req):
        base_directory=os.path.dirname(req.uri);
        base_directory=base_directory.strip("/")
        base_path=os.path.join(req.document_root(),base_directory)
        return base_path
def log(req):
	newlines="No LOG";
	try:
		db_path=os.path.join(_base_directory(req),"data/verblog.db")
		conn = sqlite.connect(db_path)
		c = conn.cursor()
		c.execute('select * from verblog order by timelog DESC')
		newlines=u"";
		for row in c:
        ##    print row
			verb=row[0];
			haraka=row[1]
			transitive=row[2]
			tenses=row[3]
			timelog=row[4]
			unvocalised=row[5]
			timelog=timelog[:timelog.rfind(":")]
			newlines+=";".join([timelog,verb,haraka,transitive,tenses,unvocalised,"<br/>"]);
		c.close();
	except:
		newlines="No Log"+req.document_root()+"-"+base_directory
	return _any_page(req, 'log',newlines)

def home(req):
    return _any_page(req, 'home')

def conjugate(req, result=u"",suggest=u""):
    return _result_page(req, 'conjugate', result,suggest)

def _error(req, word,suggest):
    ret=_error_page(req, 'error', word,suggest)
    return json.dumps({"error":"Error","word":word,"suggest":suggest},ensure_ascii=False);

def help(req):
    return _any_page(req, 'help')

def idea(req):
    return _any_page(req, 'idea')

def download(req):
    return _any_page(req, 'download')
def link(req):
    return _any_page(req, 'link')

def contact(req):
    return _any_page(req, 'contact')

def changlog(req):
    return _any_page(req, 'changlog')

def qutrub(req):
    return _any_page(req, 'qutrub')

##
# Internal functions. Because they begin with an underscore,
# the publisher will not allow them to be accessible from the
# web. Perhaps a cleaner technique is to place these into a
# seprate module. If that module contains __access__ = 0 global
# variable, then none of its contents would be accessible via
# the publisher, in which case you don't need to prepend
# underscores to function names.

def _base_url(req, ssl=0):
    """
    This function makes its best effort to guess the base url.
    Sometimes it is simpler to just hard code the base url as
    a constant, but doing something like this makes the application
    independent of what the name of the site is.
    """

    # first choice is the 'Host' header, second is the
    # hostname in the Apache server configuration.
    host = req.headers_in.get('host', req.server.server_hostname)

    # are we running on an unusual port?
    if not ':' in host:
        port = req.connection.local_addr[1]
        if port != 80 and not ssl:
            host = "%s:%d" % (host, req.connection.local_addr[1])

    # SSL?
    if ssl:
        base = 'https://' + host + req.uri
    else:
        base = 'http://' + host + req.uri

    # chop off the last part of the URL
    return os.path.split(base)[0]

def _tmpl_path(name):
    """ A shorthand for building a full path to a template """
    return os.path.join(TMPL_DIR, name)

def _menu(req, hlight, tmpl=MENU_TMPL):
    """
    This function builds a menu. The actual menu contents is inside the
    PSP template whose file name is passed in as tmpl. The hlight string
    gives the menu an opportunity to highlight a particular menu item.
    """

    tmpl = _tmpl_path(tmpl)
    m = psp.PSP(req, tmpl, vars={'hlight':hlight})
    return m

def _any_page(req, name, result=u""):
    """
    Construct a web page given a page name, which is a string identifier
    that is also passed to the _menu() function to highlight the current
    menu item, and is used to construct the filename for the page template.
    """

    body_tmpl = _tmpl_path('%s_body.html' % name)
#    form = cgi.FieldStorage() # instantiate only once!
#    name = req.form.getfirst('verb', '')
    image_path = os.path.join(_base_url(req),"images/logo.jpg")
    vars = {
            "image":	image_path,
            "name":	name,
            "result":	result.encode("utf8"),
            "body":	psp.PSP(req, body_tmpl)
            }

    main_tmpl = _tmpl_path(MAIN_TMPL)

    return psp.PSP(req, main_tmpl, vars=vars)



def _result_page(req, name, result=u"",suggest=u""):
    """
    Construct a web page given a page name, which is a string identifier
    that is also passed to the _menu() function to highlight the current
    menu item, and is used to construct the filename for the page template.
    """

    display_format = req.form.getfirst('display_format', 'HTML')
    body_tmpl = _tmpl_path('tasrefat.%s'%display_format.lower())
    vars = {
	"result": result.encode("utf8"),
	"suggestion":	suggest.encode("utf8")}

    body = psp.PSP(req, body_tmpl, vars=vars)
    # XXX this is a hack
    if display_format == "HTML":
        image_path=os.path.join(_base_url(req),"images/logo.jpg")
        vars = {
            "image":	image_path,
            "name":	name,
            "result":	body,
            "suggestion":	suggest,
            "body":	""
            }
        #main_tmpl = _tmpl_path(MAIN_TMPL)
        #return psp.PSP(req, main_tmpl, vars=vars)
    elif display_format == "CSV":
		req.headers_out["Content-type"] = "text/csv;charset=utf-8"
		req.headers_out["Content-Disposition"] = "attachment"
    elif display_format == "TeX":
		req.headers_out["Content-type"] = "text/vnd.latex-z;charset=utf-8"
		req.headers_out["Content-Disposition"] = "attachment"
    elif display_format == "XML":
		req.headers_out["Content-type"] = "text/xml;charset=utf-8"
		req.headers_out["Content-Disposition"] = "attachment"
    return body



def _error_page(req, name, word=u"",suggest=[]):


    body_tmpl = _tmpl_path('error_body.html')
    vars = {
    "word": word.encode("utf8"),
	"suggestion":	suggest.encode("utf8")
	}

    body = psp.PSP(req, body_tmpl, vars=vars)
    # XXX this is a hack
    image_path=os.path.join(_base_url(req),"images/logo.jpg")
    vars = {
        "image":	image_path,
        "name":	name,
        "result":	body,
        "suggestion":	suggest,
        "body":	""
        }
#    main_tmpl = _tmpl_path(MAIN_TMPL)
    #return psp.PSP(req, main_tmpl, vars=vars)
    return body

