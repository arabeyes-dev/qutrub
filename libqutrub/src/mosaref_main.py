﻿#************************************************************************
# $Id: mosaref_main.py,v 0.7 2009/06/02 01:10:00 Taha Zerrouki $
#
# ------------
# Description:
# ------------
#  Copyright (c) 2009, Arabtechies, Arabeyes Taha Zerrouki
#
#  This file is used by the web interface to execute verb conjugation
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date: 2009/06/02 01:10:00 $
#  $Author: Taha Zerrouki $
#  $Revision: 0.7 $
#  $Source: arabtechies.sourceforge.net
#
#***********************************************************************/
import sys,re,string
import sys, os
from classverb import *

create_index_triverbtable()
def get_future_form(verb_vocalised,haraka):

    word=verb_vocalised
    transitive=True;
    future_type=haraka
    if future_type not in (FATHA,DAMMA,KASRA):
        future_type=get_future_type_by_name(future_type);
    vb=verbclass(word,transitive,future_type);
    #vb.verb_class();
    return vb.conjugate_tense_pronoun(TenseFuture,PronounHuwa);

def do_sarf(word,future_type,all=True,past=False,future=False,passive=False,imperative=False,future_moode=False,confirmed=False,transitive=False,display_format="HTML"):
	valid=is_valid_infinitive_verb(word)
	if valid:
		future_type=get_future_type_by_name(future_type);
		bab_sarf=0;
		#init the verb class to treat the verb
		vb=verbclass(word,transitive,future_type);
		vb.set_display(display_format);

		if all :
			result= vb.conjugate_all_tenses();
		else :
			listetenses=[];
			if past : listetenses.append(TensePast);
			if (past and passive ) : listetenses.append(TensePassivePast)
			if future : listetenses.append(TenseFuture);
			if (future and passive ) : listetenses.append(TensePassiveFuture)
			if (future_moode) :
				listetenses.append(TenseSubjunctiveFuture)
				listetenses.append(TenseJussiveFuture)
			if (confirmed) :
				if (future):listetenses.append(TenseConfirmedFuture);
				if (imperative):listetenses.append(TenseConfirmedImperative);
			if (future and transitive and confirmed) :
				listetenses.append(TensePassiveConfirmedFuture);
			if (passive and future_moode) :
				listetenses.append(TensePassiveSubjunctiveFuture)
				listetenses.append(TensePassiveJussiveFuture)
			if imperative : listetenses.append(TenseImperative)
			result =vb.conjugate_all_tenses(listetenses);
		return result;
	else: return None;




