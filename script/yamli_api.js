﻿// Copyright (c) 2009 Language Analytics LLC //
// Build 4424 //
function Yamlieval2(txt){return eval("("+txt+")")}if(!window.Yamli){(function(){var q=function(aW){return document.createElement(aW)
},n=function(aW){return document.getElementById(aW)},aA=function(aW){return document.getElementsByTagName(aW)
},aq=encodeURIComponent,aL,s=false,az=true,j="div",ap="block",aH="none",ag="absolute",am=document.documentElement,g,z={},H;
window.Yamli=z;z.I={};z.I.buildNumber="4413";z.isDebug=az;z.debug=function(aW){if(typeof window.debug==="undefined"){return
}window.debug(aW)};var ay=0,aG=1,t=2,aE=3,aK=4,D=5,F,l,B,c,o,k,aR,w,v,K=0,at=0,e={en:{dir:"ltr",show_more:"show more",report_word:"report word",report_word_hint:"Click to report that your word choice doesn't appear",powered:'Powered by <a class="yamliapi_anchor" target="yamli_win" href="http://www.yamli.com">Yamli.com</a>',settings_link:"Yamli 3arabi",settings_link_hint:"Change Yamli Arabic typing settings",quick_toggle_on:"on",quick_toggle_on_hint:"Click to enable Arabic conversion",quick_toggle_off:"off",quick_toggle_off_hint:"Click to disable Arabic conversion",settings_title:"Yamli Settings",close:"close",settings_close_hint:"Close settings",enable_link:"Turn Arabic on",enable_link_hint:"Click to enable Arabic conversion",disable_link:"Turn Arabic off",disable_link_hint:"Click to disable Arabic conversion",align_left_link:"Type left to right",align_left_link_hint:"Click if you are writing mostly in English",align_right_link:"Type right to left",align_right_link_hint:"Click if you are writing mostly in Arabic",tips_link:"Quick Tips",tips_link_hint:"Quick tips to get your started",help_link:"Tutorial",help_link_hint:"Learn how to use Yamli",tips_title:"Yamli Quick Tips",tips_close_hint:"Close quick tips",hint_close:"Don't show this again",hint_content_start:'You can now <span style=\'font-weight:bold\'>type Arabic</span> using <a class="yamliapi_anchor_y" target="yamli_win" href="http://www.yamli.com">Yamli</a> !',hint_content_try:"<span style='text-decoration:underline'>Try it now!</span>",hint_content_end:"Look for the [Y] logo for more settings.<br/>",hint_close_hint:"Close",loading:"Loading...",sponsored:"Sponsored by",def_pick_hint:"Press <span style='font-weight:bold'>SPACE</span> to automatically<br/>pick the best word"},fr:{dir:"ltr",show_more:"plus de choix",report_word:"mot manquant",report_word_hint:"Cliquez pour nous informer que le mot voulu manque",powered:'motorisé par <a class="yamliapi_anchor" target="yamli_win" href="http://www.yamli.com">Yamli.com</a>',settings_link:"Yamli 3arabi",settings_link_hint:"Changez vos préférences",quick_toggle_on:"actif",quick_toggle_on_hint:"Activez la conversion en Arabe",quick_toggle_off:"inactif",quick_toggle_off_hint:"Désactivez la conversion en Arabe",settings_title:"Préférences Yamli",close:"fermer",settings_close_hint:"Fermez les préférences",enable_link:"Activer l'Arabe",enable_link_hint:"Activez la conversion en Arabe",disable_link:"Désactivez l'Arabe",disable_link_hint:"Désactivez la conversion en Arabe",align_left_link:"Taper de gauche à droite",align_left_link_hint:"Cliquez si vous écrivez surtout en Français",align_right_link:"Taper de droite à gauche",align_right_link_hint:"Cliquez si vous écrivez surtout en Arabe",tips_link:"Aide rapide",tips_link_hint:"Conseils rapides pour rapidement utiliser Yamli",help_link:"Aide detailée",help_link_hint:"Apprenez à utiliser Yamli",tips_title:"Aide Rapide pour Yamli",tips_close_hint:"Fermer l'aide rapide",hint_close:"Ne plus afficher",hint_content_start:'<span style=\'font-weight:bold\'>Tape en Arabe</span> avec <a class="yamliapi_anchor_y" target="yamli_win" href="http://www.yamli.com/fr/">Yamli</a> !',hint_content_try:"<span style='text-decoration:underline'>Essaye!</span>",hint_content_end:"Clique sur le logo [Y] pour les préférences.<br/>",hint_close_hint:"Fermer",loading:"Chargement...",sponsored:"Sponsorisé par",def_pick_hint:"Tape <span style='font-weight:bold'>ESPACE</span> pour automatiquement<br/>choisir le meilleur mot"},ar:{dir:"rtl",show_more:"اعرض المزيد",report_word:"كلمة ناقصة",report_word_hint:"انقر إذا لم تجد كلمتك",powered:'Powered by <a class="yamliapi_anchor" target="yamli_win" href="http://www.yamli.com">Yamli.com</a>',settings_link:"Yamli 3arabi",settings_link_hint:"خيارات",quick_toggle_on:"نشط",quick_toggle_on_hint:"انقر لتفعيل العربية",quick_toggle_off:"غير نشط",quick_toggle_off_hint:"انقر لتعطيل العربية",settings_title:"Yamli خيارات",close:"أغلق",settings_close_hint:"أغلق الخيارات",enable_link:'<span style="display:block;text-align:right">تفعيل العربية</span>',enable_link_hint:"انقر لتفعيل العربية ",disable_link:'<span style="display:block;text-align:right">تعطيل العربية</span>',disable_link_hint:"انقر لتعطيل العربية",align_left_link:'<span style="display:block;text-align:right">من اليسار إلى اليمين</span>',align_left_link_hint:"انقر هنا إن كنت تكتب معظماً باللغة الإنكليزية",align_right_link:'<span style="display:block;text-align:right">من اليمين إلى اليسار</span>',align_right_link_hint:"انقر هنا إن كنت تكتب معظماً باللغة العربية",tips_link:'<span style="display:block;text-align:right">نصائح سريعة</span>',tips_link_hint:"نصائح سريعة",help_link:'<span style="display:block;text-align:right">مساعدة</span>',help_link_hint:"Yamli تعلم كيفية إستعمال",tips_title:"نصائح سريعة",tips_close_hint:"أغلق نافذة النصائح السريعة",hint_close:"لا تظهر مرة اخرى",hint_content_start:'يمكنك الآن <span style=\'font-weight:bold\'>طباعة العربية</span> باستخدام <a class="yamliapi_anchor_y" target="yamli_win" href="http://www.yamli.com/ar/">يملي</a> !',hint_content_try:"<span style='text-decoration:underline'>جرّب الآن!</span>",hint_content_end:"أبحث عن هذا الشعار [Y] لمزيد من الخيارات.<br/>",hint_close_hint:"Close",loading:"Loading...",sponsored:"برعاية",def_pick_hint:"إضغط على <span style='font-weight:bold;white-space:nowrap'>المسافة (SPACE)</span><br/>لإختيار الكلمة الأفضل تلقائياً"}};
var al="#112abb",I="#5d157e",y="#dde4ee",R="#f5f9ff",Q="#c6d8ff",aB="black",ax="black",ah=al,aP="0000cc",M="3px",aw="11px",ad="#edf1f7",aQ="#e1e4ea",aD="useRomanNum",aF;
z.I.setUnsupported=function(aW){l=D;aF=aW;if(aW===aL){aF='Yamli is not supported on this web browser.<br />We recommend using <a href="http://getfirefox.com/">Firefox</a>.'
}};var aS=function(){F=document.compatMode=="BackCompat";l=ay;var aW=navigator.userAgent.toLowerCase(),aX;
if((aX=aW.match(/msie ([0-9])\.([0-9])/i))){K=parseInt(aX[1],10);at=parseInt(aX[2],10);
if(K<6||aW.indexOf("iemobile")!=-1){z.I.setUnsupported();return}l=aG;o=1}if((aX=aW.match(/safari\/([0-9]+)\.([0-9]+)/i))){K=parseInt(aX[1],10);
at=parseInt(aX[2],10);if(K<525){z.I.setUnsupported('Yamli requires <a href="http://www.apple.com/safari/">Safari 3.1</a> or better');
return}l=aE;k=1;if(aW.indexOf("chrome")!=-1){v=1}else{w=1}}else{if((aX=aW.match(/firefox\/([0-9]+)\.([0-9]+)/i))){K=parseInt(aX[1],10);
at=parseInt(aX[2],10);c=B=1;l=t}else{if((aW.indexOf("gecko")!=-1)){if(aW.indexOf("gecko/20020924 aol/7.0")!=-1){z.I.setUnsupported();
return}l=t;B=1}else{if((aX=aW.match(/opera\/([0-9]+)\.([0-9]+)/i))){K=parseInt(aX[1],10);
at=parseInt(aX[2],10);if(K<9||(K==9&&at<=2)){z.I.setUnsupported('Yamli is not supported on Opera version earlier than 9.5.<br />Please upgrade to the <a href="http://www.opera.com/download/">latest Opera</a>, or use <a href="http://getfirefox.com/">Firefox</a>.');
return}l=aK;aR=1}}}}};z.DomReady=function(){var a0=s,aX=[],aW,aZ=function(){if(a0){return
}a0=az;for(var a1=0;a1<aX.length;++a1){aX[a1]()}aX=aL},aY=function(){var a1=aZ;if(o){aW=setInterval(function(){var a2=q("p");
try{a2.doScroll("left")}catch(a3){return}clearInterval(aW);aW=aL;a2=aL;a1()},50)}else{z.I.addEvent(document,"DOMContentLoaded",a1)
}};this.addCallback=function(a1){if(a0){a1();return}aX.push(a1)};this.unload=function(){if(o){if(aW){clearInterval(aW);
aW=aL}}else{z.I.removeEvent(document,"DOMContentLoaded",aZ)}};aY()};var G=function(aZ,aY,aX){var aW=aZ.slice((aX||aY)+1||aZ.length);
aZ.length=aY<0?aZ.length+aY:aY;return aZ.push.apply(aZ,aW)},ab=function(aW,aX){if(o){aW.style.styleFloat=aX
}else{aW.style.cssFloat=aX}},p=function(aW){while(aW){if(aW==g){return az}aW=aW.parentNode
}return s};z.I.setCookie=function(aX,a0,aZ,aY){var aW=new Date();aW.setDate(aW.getDate()+aZ);
document.cookie=aq(aX)+"="+aq(a0)+(";expires="+aW.toGMTString())+(";path=/")+(aY?";domain="+aY:"")
};z.I.getCookie=function(aY){if(document.cookie.length>0){var aW=aq(aY),aZ=document.cookie.indexOf(aW+"=");
if(aZ!=-1){aZ=aZ+aW.length+1;var aX=document.cookie.indexOf(";",aZ);if(aX==-1){aX=document.cookie.length
}return decodeURIComponent(document.cookie.substring(aZ,aX))}}return aL};var C=function(aW){var aY=aL;
if(typeof aW=="string"){aY=n(aW);if(!aY){var aX=document.getElementsByName(aW);if(aX.length==1){aY=aX[0]
}}}return aY},ao=function(aW,aX){while(aW){if(aW==aX){return az}aW=aW.parentNode}return s
},au=function(){var aX=[],aW=location.hash.substr(1);if(aW.length===0){return aX}aW=aW.replace(/\+/g," ");
var aZ=aW.split("&"),aY;for(aY=0;aY<aZ.length;++aY){var a2=aZ[aY].split("="),a0=decodeURIComponent(a2[0]),a1=a0;
if(a2.length==2){a1=decodeURIComponent(a2[1])}aX[a0]=a1}return aX},r=function(){if(typeof window.pageYOffset=="number"){return window.pageXOffset
}else{if(g&&(g.scrollLeft||g.scrollTop)){return g.scrollLeft}}return am.scrollLeft
},aM=function(aW){var aX=q("style");aX.type="text/css";if(aX.styleSheet){aX.styleSheet.cssText=aW
}else{var aY=document.createTextNode(aW);aX.appendChild(aY)}aA("head")[0].appendChild(aX);
return aX},m=function(aW){var aX=aA("head")[0];if(aW.parentNode==aX){aX.removeChild(aW)
}};z.I.trimString=function(aY){aY=aY.replace(/^\s\s*/,"");var aW=/\s/,aX=aY.length;
while(aW.test(aY.charAt(--aX))){}return aY.slice(0,aX+1)};z.I.addEvent=function(aZ,aY,aW){if(aZ.addEventListener){aZ.addEventListener(aY,aW,s);
return az}else{if(aZ.attachEvent){var aX=aZ.attachEvent("on"+aY,aW);return aX}}};
z.I.removeEvent=function(aZ,aY,aW){if(aZ.removeEventListener){aZ.removeEventListener(aY,aW,s);
return az}else{if(aZ.detachEvent){var aX=aZ.detachEvent("on"+aY,aW);return aX}}};
var ae=function(aW){if(aW.preventDefault){aW.preventDefault()}else{if(window.event){window.event.returnValue=s
}else{return s}}},L=function(aX,aY){var aW;if(aX.fireEvent){aW=document.createEventObject();
aX.fireEvent("on"+aY,aW)}else{if(aX.dispatchEvent){aW=document.createEvent("HTMLEvents");
aW.initEvent(aY,az,az);aX.dispatchEvent(aW)}}},aC=function(aY){var aW=(aY)?aY:window.event,aX=aW.charCode?aW.charCode:(aW.keyCode?aW.keyCode:(aW.which?aW.which:0));
if(k){switch(aX){case 63232:return 38;case 63233:return 40;case 63234:return 37;case 63235:return 39
}}return aX},av=function(aY){var aW=(aY)?aY:window.event,aX;if(aR){aX=aW.which}else{aX=(aW.charCode!==aL)?aW.charCode:((aW.keyCode!==aL)?aW.keyCode:((aW.which!==aL)?aW.which:0));
if(aX===0&&aW.keyCode===13){aX=13}}return aX},P=function(aX){var aW=(aX)?aX:window.event;
return(typeof aW.shiftKey!="undefined"&&aW.shiftKey)},aj=function(aX){var aW={x:0,y:0};
if(typeof aX.pageX!="undefined"){aW.x=aX.pageX;aW.y=aX.pageY}else{if(typeof aX.clientX!="undefined"){aW.x=aX.clientX+am.scrollLeft||g.scrollLeft||0;
aW.y=aX.clientY+am.scrollTop||g.scrollTop||0}}return aW},aV=function(aW){return(aW.which!==aL)?aW.which:aW.button
},aa=function(aW){return aW.srcElement||aW.target},J=function(aW){if(window.getComputedStyle){return window.getComputedStyle(aW,aL)
}if(aW.currentStyle){return aW.currentStyle}return document.defaultView.getComputedStyle(aW,aL)
};z.I.WordType={EMPTY:0,PURE_ROMAN:1,PURE_NUMBER:2,PURE_ARABIC:3,ROMAN_ARABIC:4,EXCLUDED:5,MIXED:6};
var ar={" ":" ","\xa0":"\xa0",",":"\u060c",";":"\u061b","?":"\u061f","\u060c":"\u060c","\u061b":"\u061b","\u061f":"\u061f",".":".",'"':'"',"(":"(",")":")","{":"{","}":"}","[":"[","]":"]","!":"!","$":"$","&":"&","*":"*","+":"+","-":"-","/":"/",":":":","<":"<",">":">","@":"@","~":"~","`":"`","^":"^","=":"=","\\":"\\","\r":"\r","\n":"\n"},aJ={"0":"\u0660","1":"\u0661","2":"\u0662","3":"\u0663","4":"\u0664","5":"\u0665","6":"\u0666","7":"\u0667","8":"\u0668","9":"\u0669","%":"\u066a",".":"\u066b",",":"\u066c"},A=new RegExp("^['0-9a-zA-Z\\xc0-\\xd6\\xd8-\\xf6\\xf8-\\xff]*$"),ai=new RegExp("^(([0-9,]*)|([0-9]*\\.[0-9]+))%?$"),Z=new RegExp("^['0-9a-zA-Z\\xc0-\\xd6\\xd8-\\xf6\\xf8-\\xff\\u0600-\\u06ff]*$"),f=new RegExp("^(AND|OR)$|^http:|^ftp:|@|^www.|^site:|^cache:|^link:|^related:|^info:|^stocks:|^allintitle:|^intitle:|^allinurl:|^inurl:"),x=new RegExp("^(al|Al|AL|el|El|EL|il|Il|IL|wa|Wa|WA|w|W|be|Be|BE|bi|Bi|BI|b|B|l|L|lel|lil|ll|fa|wal|wel|sa|bel|bil|le|li|fal|fil|fel)$"),aN=new RegExp("^\\s$"),V=new RegExp("\\s"),b=new RegExp("(<[^<>]*)$|(\\[[^\\[\\]]*)$");
z.I.HasPunctuationRegexp=new RegExp('[ \\r\\n,;\\.\\"\\?\\(\\){}\\[\\]!\\$&\\*\\+\\-/:<>@~`\\^=\\\\\\u060c\\u061b\\u061f\\u06d4\\xa0]');
z.I.PureArabicRegexp=new RegExp("^[\\u0600-\\u06ff]*$");var aI=1,h=2,S=3,O=4,u=5,ac=6,U=7,aU=function(aW){if(aW.length===0){return z.I.WordType.EMPTY
}if(aW.match(f)){return z.I.WordType.EXCLUDED}if(aW.match(ai)){return z.I.WordType.PURE_NUMBER
}if(aW.match(z.I.PureArabicRegexp)){return z.I.WordType.PURE_ARABIC}if(aW.match(A)){return z.I.WordType.PURE_ROMAN
}if(aW.match(Z)){return z.I.WordType.ROMAN_ARABIC}return z.I.WordType.MIXED},af=function(a9){var ba={start:0,end:0};
if(a9.setSelectionRange){ba.start=a9.selectionStart;ba.end=a9.selectionEnd}else{if(document.selection&&document.selection.createRange){if(document.activeElement!=a9){a9.focus()
}var a1=document.selection.createRange(),a0=a1.duplicate();if(a9.type=="textarea"){var a3=g.createTextRange();
a3.moveToElementText(a9);a3.setEndPoint("EndToStart",a0);var aY=g.createTextRange();
aY.moveToElementText(a9);aY.setEndPoint("StartToEnd",a0);var a4=s,aX=s,a8=s,a6,a2,a7,aW,a5,aZ;
a6=a2=a3.text;a7=aW=a0.text;a5=aZ=aY.text;do{if(!a4){if(a3.compareEndPoints("StartToEnd",a3)===0){a4=az
}else{a3.moveEnd("character",-1);if(a3.text==a6){a2+="\r\n"}else{a4=az}}}if(!aX){if(a0.compareEndPoints("StartToEnd",a0)===0){aX=az
}else{a0.moveEnd("character",-1);if(a0.text==a7){aW+="\r\n"}else{aX=az}}}if(!a8){if(aY.compareEndPoints("StartToEnd",aY)===0){a8=az
}else{aY.moveEnd("character",-1);if(aY.text==a5){aZ+="\r\n"}else{a8=az}}}}while((!a4||!aX||!a8));
ba.start=a2.length;ba.end=ba.start+aW.length}else{ba.start=0-a0.moveStart("character",-100000);
ba.end=ba.start+a1.text.length}}}return ba};z.I.loadScript=function(aZ,aX,a2,aY){var aW=q("script");
aW.type="text/javascript";aW.charset="utf-8";if(aX){if(o){aW.text=aZ}else{aW.appendChild(document.createTextNode(aZ))
}}else{aW.src=aZ}var a1=function(){aW.onload=null;if(!a2){aW.parentNode.removeChild(aW)
}aW=aL;if(aY){aY()}};var a0=function(a3){if(aW.readyState=="loaded"||aW.readyState=="complete"){aW.onreadystatechange=null;
a1()}};switch(l){case t:case aE:aW.onload=a1;break;default:aW.onreadystatechange=a0;
break}aA("head")[0].appendChild(aW)};z.I.SXHRData={_pendingRequests:{},_counter:1,reserveId:function(){return this._counter++
},start:function(aX,aW,aY){if(aY===aL){aY=this._counter++}this._pendingRequests[aY]=aX;
z.I.loadScript(aW.replace("%ID%",aY))},dataCallback:function(aW){if(!aW||!aW.hasOwnProperty("id")||!aW.hasOwnProperty("data")){return
}var aX=this._pendingRequests[aW.id];delete this._pendingRequests[aW.id];aX._responseCallback(aW.data)
},unload:function(){for(var aW=0;aW<this._pendingRequests.length;++aW){this._pendingRequests[aW].unload()
}this._pendingRequests=aL}};z.I.SXHR=function(){var aY,aW="&sxhr_id=%ID%",aX;this.reserveId=function(){aX=z.I.SXHRData.reserveId();
return aX};this.open=function(a0,aZ){aY=a0;if(aZ===s){aW=""}else{if(aZ){aW=aZ}}};
this.onreadystatechange=function(){};this.send=function(){z.I.SXHRData.start(this,aY+aW,aX)
};this.readyState=0;this.status=0;this.responseText=aL;this._responseCallback=function(aZ){this.responseText=aZ;
this.status=200;this.readyState=4;this.onreadystatechange()};this.unload=function(){this._responseCallback=function(){};
this.onreadystatechange=aL}};z.I.createSXHR=function(){return new z.I.SXHR()};z.I.createXHR=function(){if(typeof XMLHttpRequest!="undefined"){return new XMLHttpRequest()
}else{if(typeof window.ActiveXObject!="undefined"){return new window.ActiveXObject("Microsoft.XMLHTTP")
}else{throw new Error("XMLHttpRequest not supported")}}};z.I.getDimensions=function(aZ){var aY=0,a1=0,aW=aZ.offsetWidth,a5=aZ.offsetHeight,a3=0;
if(o&&aZ.getBoundingClientRect){var a0=(am&&am.clientWidth===0)?g:am,aX=aZ.getBoundingClientRect();
aY=aX.left-a0.clientLeft+a0.scrollLeft;a1=aX.top-a0.clientTop+a0.scrollTop;a3=a0.scrollWidth-(aX.right-a0.clientLeft+a0.scrollLeft)
}else{if(aZ.offsetParent){var a2=aZ;while(a2.offsetParent){if(a2!=g){aY-=a2.scrollLeft;
a1-=a2.scrollLeft}if(a2.tagname==="table"||J(a2).display=="table"){var a4=window.getComputedStyle(a2,"");
aY+=parseFloat(a4.borderLeftWidth)||0;a1+=parseFloat(a4.borderTopWidth)||0}aY+=a2.offsetLeft;
a1+=a2.offsetTop;a2=a2.offsetParent}if(a2!==aL){aY+=a2.offsetLeft;a1+=a2.offsetTop
}}else{if(aZ.x){aY+=aZ.x;a1+=aZ.y}}}return{left:aY,top:a1,width:aW,height:a5,right:a3}
};var T=function(a3,a4,a2,aW){var aY=a4.start,a1=a4.end;while(aY>0&&!a3.charAt(aY-1).match(z.I.HasPunctuationRegexp)){aY--
}while(a1<a3.length&&!a3.charAt(a1).match(z.I.HasPunctuationRegexp)){a1++}var aX=a3.substring(aY,a1);
if(aX.match(z.I.HasPunctuationRegexp)||aX.length===0){return aL}if(aW&&a3.substr(0,aY).match(b)){return aL
}var a0=aU(aX),aZ={word:aX,start:aY,end:a1,selectionOffset:a4.start-aY,selectionStart:a4.start,selectionEnd:a4.end,isEdge:(aY==a4.start||a1==a4.end),wordType:a0};
if(!a2){aZ=H.convertDashedWordSelection(aZ)}return aZ},i=function(aZ,a0,a3,aW){var a2=a0.start,aY=a0.end;
while(a2>0&&aZ.charAt(a2-1)!=" "){a2--}while(aY<aZ.length&&aZ.charAt(aY)!=" "){aY++
}var a1=aZ.substring(a2,aY);if(a1.indexOf(" ")!=-1||a1.length===0){return aL}var aX=aU(a1);
if(aX==z.I.WordType.PURE_NUMBER||aX==z.I.WordType.EXCLUDED){return{word:a1,start:a2,end:aY,selectionOffset:a0.start-a2,selectionStart:a0.start,selectionEnd:a0.end,isEdge:(a2==a0.start||aY==a0.end),wordType:aX}
}return T(aZ,a0,a3,aW)},W=function(aW){var aX={value:0,unit:""};if(aW.length===0){return aX
}var aY=aW.substr(aW.length-2);if(aY.charAt(1)=="%"){aY="%"}aX.unit=aY;aX.value=aW.substr(0,aW.length-aY.length);
return aX},Y=function(aW){return parseInt(W(aW).value,10)},aT=function(aW){return parseFloat(W(aW).value,10)
},a=function(aW,aX){aW.innerHTML=aX},X=function(a2,aX,aZ){var aW=[],a3=[],a4=[],a5,a0,a6=s,a1;
for(var aY=0;aY<a2.length;++aY){a0=a2.charAt(aY);a5=a2.charCodeAt(aY);if(aY==aX){if(a6){a4.push(a0);
a3=[];a3.push(a4.join(""));a4=[];continue}else{aW.push(a0);break}}if(a5==32){if(a6){a3.push(a4.join(""));
a4=[];a3.push(a0)}else{aW.push(a0)}continue}a1=a5>=1536&&a5<=1791;if((a1&&!aZ)||(!a1&&aZ)){a4.push(a0);
a6=az;continue}if(a6){a3.push(a4.join(""));a4=[];a3.reverse();while(a3.length>0){aW.push(a3.pop())
}a6=s}if(!a6&&aY>aX){break}aW.push(a0)}if(a6){if(aY==aX){a3=[]}a3.push(a4.join(""));
a3.reverse();while(a3.length>0){aW.push(a3.pop())}}return aW.join("")},aO=function(){var aW=0,aX=-1,aY=function(){if(aX==-1){return 0
}return new Date().getTime()-aX};this.start=function(){if(aX!=-1){return}aX=new Date().getTime()
};this.stop=function(){aW+=aY();aX=-1};this.reset=function(){aX=-1;aW=0};this.getElapsed=function(){return aW+aY()
}};z.I.Draggable=function(a4,a7){var a5=a4,aX=a7,a3,a2,a9,a8,a0={IDLE:0,DRAGGING:1},aW=a0.IDLE,a1=function(){a5.style.cursor="move";
a7.style.position=ag;z.I.addEvent(a5,"mousedown",aZ);z.I.addEvent(a5,"mouseup",aY)
},aZ=function(bb){var bd=aa(bb);if(!bd){return}while(bd&&bd!=a5){if(bd.className.indexOf("yamliNoDrag")!=-1){return
}bd=bd.parentNode}var bc=aV(bb);if(bc==1){var ba=aj(bb);a3=ba.x;a2=ba.y;a9=Y(H.useRightLayout()?aX.style.right:aX.style.left);
a8=Y(aX.style.top);aW=a0.DRAGGING;z.I.addEvent(document,"mousemove",a6);return ae(bb)
}},a6=function(bb){if(aW==a0.DRAGGING){var be=aV(bb);if(!(be==1)){aY();return ae(bb)
}var ba=aj(bb);if(ba.x<0){ba.x=0}if(ba.y<0){ba.y=0}var bd=ba.x-a3,bc=ba.y-a2;if(H.useRightLayout()){aX.style.right=(a9-bd)+"px"
}else{aX.style.left=(a9+bd)+"px"}aX.style.top=(a8+bc)+"px";return ae(bb)}},aY=function(ba){if(aW==a0.DRAGGING){if(ba===aL||aV(ba)==1){aW=a0.IDLE;
z.I.removeEvent(document,"mousemove",a6);if(ba){return ae(ba)}}}};this.unload=function(){z.I.removeEvent(a5,"mousedown",aZ);
z.I.removeEvent(a5,"mouseup",aY)};a1()};var d=function(aY){var cx={x:0,xr:0,y:0},bo,bA,b4,bg,bf,br,cs={x:0,xr:0,y:0},a0,ck,cm,bY={x:0,xr:0,y:0},cr,bX,bz,cj,b1,bO,bD,a1,bN,cA,a8,bF={x:0,xr:0,y:0,yr:0},bu,bd,cb,bM,bI=[],cg=[],co=[],bv=[],ch,cp,bi=aY,bR,bw,bm,by=s,b0,bB,bb=s,aX=az,bc,a6,bs="yamliapi_inst_ff_"+aY.getInput().id,a5,bk=this,bn=function(){var cH=H.getOption("zIndexBase");
br=q(j);br.className="yamliapi_settingsDiv";br.style.position=ag;br.style.visibility="hidden";
H.appendChild(br);bO=bi.getSettingsButtonSize();br.style.paddingLeft=bO+"px";ck=cv("","settings_link_hint",bG);
z.I.addEvent(ck,"dblclick",be);br.appendChild(ck);if(bO){b1=q(j);b1.style.width=b1.style.height=bO+"px";
b1.style.display=ap;b1.style.position=ag;var cQ=H.makeUrl("/cache_safe/logo_y_"+bO+".gif","?");
b1.style.background="url('"+cQ+"') no-repeat";b1.style.backgroundPosition="left 0px";
b1.style.left="0";b1.style.top=bi.getSettingsButtonOffset()+2+"px";ck.appendChild(b1)
}a0=q("span");br.appendChild(a0);a0.appendChild(cC("&nbsp;"));var cP=cv("","settings_link_hint",bG);
a0.appendChild(cP);cP.appendChild(cC("settings_link"));a0.appendChild(cC("&nbsp;("));
cm=cv("","",bP);a0.appendChild(cm);a0.appendChild(cC(")"));ce(a0,"fontSize",bi.getSettingsFontSize());
ce(a0,"color",bi.getOption("settingsColor"));ce(cm,"color",bi.getOption("settingsLinkColor"));
ce(cP,"color",bi.getOption("settingsLinkColor"));cr=q(j);cr.className="yamliapi_menuBorder";
cr.style.position=ag;cr.style.zIndex=cH+5;cr.style.display=aH;H.appendChild(cr);b6(cr);
bX=q(j);bX.className="yamliapi_menuPanel";cr.appendChild(bX);bz=q(j);bz.className="yamliapi_menuContent";
bX.appendChild(bz);var cI=bi.lang("dir"),cK=cu(ad);cK.style.textAlign=cI=="rtl"?"right":"left";
bz.appendChild(cK);var cM=q(j);cM.style.position=ag;if(cI=="ltr"){cM.style.right="0"
}else{cM.style.left="0"}cM.style.top="0";cM.style.padding=M;var cF=cv("close","settings_close_hint",cc);
cF.style.verticalAlign="top";cM.appendChild(cF);cK.appendChild(cM);var cG=cC("close");
cG.style.visibility="hidden";if(cI=="rtl"){cK.appendChild(cG)}var cR=cC("settings_title");
cR.style.fontWeight="bold";cK.appendChild(cR);if(cI=="ltr"){cK.appendChild(cG)}var cO=a4();
bz.appendChild(cO);bD=cu();bz.appendChild(bD);bD.appendChild(cv("enable_link","enable_link_hint",be));
a1=cu();bz.appendChild(a1);a1.appendChild(cv("disable_link","disable_link_hint",be));
if(bi.getOption("showDirectionLink")){cA=cu();bz.appendChild(cA);cA.appendChild(cv("align_left_link","align_left_link_hint",cn));
bN=cu();bz.appendChild(bN);bN.appendChild(cv("align_right_link","align_right_link_hint",cn))
}cO=a4();bz.appendChild(cO);var cN=cu();cN.appendChild(cv("tips_link","tips_link_hint",b9));
bz.appendChild(cN);if(bi.getOption("showTutorialLink")){var cJ=cu(),cD=bi.getOption("tutorialUrl");
if(cD===aL){cD="http://www.yamli.com/help/";var cE=bi.getOption("uiLanguage");if(cE!="en"){cD+=cE+"/"
}cD+="?autolang=false"}var cL=b8("help_link","help_link_hint",cD,"yamli_win");cJ.appendChild(cL);
bz.appendChild(cJ)}cg.push(bz);co.push(cF);cj=ba();bz.appendChild(cj);bV(bz);a8=q(j);
a8.style.position=ag;a8.style.zIndex=cH+2;if(H.useRightLayout()){a8.style.direction="rtl"
}a8.style.display=aH;H.appendChild(a8);cb=q(j);cb.style.position=ag;cb.style.zIndex=cH+4;
if(H.useRightLayout()){cb.style.direction="rtl"}cb.style.display=aH;H.appendChild(cb);
bx();bo=q(j);bo.style.position=ag;bo.style.zIndex=cH+3;H.appendChild(bo);bA=q(j);
bA.className="yamliapi_menuBorder";bA.style.display=aH;bo.appendChild(bA);b6(bA);
b4=q(j);b4.className="yamliapi_menuPanel";bA.appendChild(b4);bg=q(j);bg.className="yamliapi_menuContent";
b4.appendChild(bg);bf=ba();b4.appendChild(bf);bV(b4);a5=aM("."+bs+" *{font-family:"+bi.getOption("uiFontFamily")+";}\n");
br.className+=" "+bs;cr.className+=" "+bs;bg.className+=" "+bs;a8.className+=" "+bs;
bl();z.I.addEvent(bg,"mouseout",aZ);z.I.addEvent(bz,"mouseout",b2)},bl=function(){var cF=bi.getSettingsMenuFontSize(),cE=bi.getCloseFontSize(),cD;
ce(a0,"fontSize",bi.getSettingsFontSize());for(cD=0;cD<cg.length;++cD){ce(cg[cD],"fontSize",cF)
}for(cD=0;cD<co.length;++cD){ce(co[cD],"fontSize",cE)}},ce=function(cD,cG,cF){if(cD.style[cG].indexOf("!important")==-1){cD.style[cG]=cF
}for(var cE=0;cE<cD.childNodes.length;++cE){if(cD.childNodes[cE].nodeType==1){ce(cD.childNodes[cE],cG,cF)
}}},b6=function(cD){if(o&&K==6&&aA("select").length>0){var cE=q("iframe");if(H.useRightLayout()){cE.style.right="0";
cE.style.marginRight="-5px"}else{cE.style.left="0";cE.style.marginLeft="-5px"}cE.className="yamli_iframeselectfix";
cD.appendChild(cE)}},bW=function(cD,cF,cE){if(H.useRightLayout()){cD.style.right=cE+"px"
}else{cD.style.left=cF+"px"}},bE=function(){while(bg.lastChild){bg.removeChild(bg.lastChild)
}},bh=function(){var cD=q("div");cD.className="yamliapi_clear";return cD},cC=function(cE){var cD=q("span");
cD.style.whiteSpace="nowrap";a(cD,bi.lang(cE));return cD},aW=function(cE){var cD=q("span");
a(cD,bi.lang(cE));return cD},cv=function(cG,cH,cF,cD){var cE=q("a");cE.className="yamliapi_simpleLink";
if(o){cE.href="javascript:void(0);"}a(cE,bi.lang(cG));if(cH){cE.title=bi.lang(cH)
}if(cF){z.I.addEvent(cE,cD?cD:"click",function(cI){cF(cI);return ae(cI)})}z.I.addEvent(cE,"mousedown",cz);
return cE},b8=function(cF,cH,cD,cG){var cE=q("a");cE.className="yamliapi_anchor";
a(cE,bi.lang(cF));cE.title=bi.lang(cH);cE.href=cD;cE.target=cG;return cE},a7=function(cH,cK,cJ,cE,cG,cI,cF,cL){var cD=q(j);
cD.style.textAlign=(cF=="rtl")?"right":"left";cD.style.padding=M;cD.style.color=cJ;
cD.style.backgroundColor=cE;cD.itemType=cH;cD.yamliTextColor=cJ;cD.yamliHighlightColor=cG;
cD.yamliMenuBackgroundColor=cE;cD.yamliMenuHighlightColor=cI;cD.style.cursor=cH==aI?"default":"pointer";
cD.style.whiteSpace="nowrap";cD.style.fontSize=Math.max(cL?(bi.getMenuFontSize()*cL):bi.getMenuFontSize(),9)+"px";
cD.style.direction=cF;a(cD,cK);z.I.addEvent(cD,"mousedown",cz);z.I.addEvent(cD,"click",bt);
z.I.addEvent(cD,"mouseover",bZ);return cD},cu=function(cD,cE){var cF=q(j);cF.style.whiteSpace="nowrap";
cF.style.padding=cE?cE:M;cF.style.backgroundColor=cD?cD:R;z.I.addEvent(cF,"mousedown",cz);
return cF},ba=function(){var cE=H.getAdInfo();if(!cE){return q(j)}var cG=cu(y);if(cE.showSponsored){var cD=q(j);
cD.style.marginBottom="1px";cD.style.textAlign="center";cD.style.whiteSpace="nowrap";
cG.appendChild(cD);a(cD,bi.lang("sponsored"));co.push(cD)}var cF=q(j);cG.appendChild(cF);
cF.style.textAlign="center";cG.ad=cF;return cG},bV=function(cF){if(H.getShowPowered()){var cE=cu(y,"0");
cE.style.textAlign="center";cE.style.padding="0 1em";cE.appendChild(cC("powered"));
if(H.getAdInfo()){co.push(cE)}cF.appendChild(cE)}else{var cD=q(j);cD.style.width="100px";
cD.style.height="1px";cD.style.fontSize="0px";cD.style.lineHeight="0px";cF.appendChild(cD)
}},bS=function(cE,cD){if(cE.ad){if(H.isAdLoaded()){bU(cE.ad)}else{bv.push(cE.ad)}cE.ad=aL
}if(cD){H.adViewInc()}},bU=function(cF){var cI=H.getAdInfo();if(!cI){return}var cH=q(j),cD=cI.width,cL=cI.height,cG;
if(!o){cH.style.margin="0 auto"}cH.style.width=cD+"px";cH.style.textAlign="center";
cF.appendChild(cH);switch(cI.adType){case"image":var cJ=q("a");cH.appendChild(cJ);
cJ.href=H.makeUrl("/sp_click.ashx?sp_id="+aq(cI.adId)+H.getReferrerInfo("&"),"&");
cJ.target="_blank";z.I.addEvent(cJ,"mouseup",function(){var cM=cJ.href.indexOf("&click_ms=");
if(cM!=-1){cJ.href=cJ.href.substr(0,cM)}cJ.href+="&click_ms="+H.getAdViewElapsed();
H.adClickCountInc()});var cE=q("img");cJ.appendChild(cE);cE.src=cI.url;break;case"iframe":cG=bL(cD,cL);
cH.appendChild(cG);cG.src=cI.url;break;case"gam":var cK=location.hash.indexOf("yamli_googleDebug")!=-1?"&google_debug":"";
cG=bL(1,0,H.getAdIframeId());cH.appendChild(cG);cG.src=cI.url+"&load=1&debugAds="+encodeURIComponent(H.debugAds)+"&hostname="+encodeURIComponent(window.location.hostname)+"&port="+window.location.port+"&pageLang="+bi.getOption("uiLanguage")+cK;
break}},a4=function(){var cD=q(j);if(o){a(cD,"&nbsp;");cD.style.fontSize="0px";cD.style.lineHeight="0px"
}else{cD.style.width="100%";cD.style.height="1px"}cD.style.borderTop="solid 1px #e0e0e0";
cD.style.backgroundColor=R;cD.style.margin="0px 0px 0px 0px";cD.itemType=h;return cD
},bL=function(cG,cE,cD){var cF=q("iframe");cF.style.display=ap;cF.width=cG+"px";cF.height=cE+"px";
cF.frameBorder="0";cF.scrolling="no";cF.allowTransparency="true";cF.src="about:blank";
if(cD){cF.name=cD;cF.id=cD}return cF},bC=function(cH,cI,cE,cG,cK){var cJ=q(j);cJ.style.whiteSpace="normal";
if(cE){cJ.style.width=cE+"px"}cJ.style.height="auto";a(cJ,'<div style="margin:1em;text-align:center">'+bi.lang("loading")+"</div>");
var cF=z.I.createSXHR(),cD=cI?cH:H.makeUrl("/ajax.ashx?path="+aq(cH),"&");cF.open(cD);
cF.onreadystatechange=function(){a(cJ,cF.responseText);if(cG){ce(cJ,"fontSize",cG)
}if(cK){cK()}};cF.send();return cJ},aZ=function(cD){var cE=aa(cD);z.I.removeEvent(cE,"mousemove",bZ);
var cG=aj(cD),cF=z.I.getDimensions(bg);if(cG.x<cF.left||cG.x>(cF.left+cF.width)||cG.y<cF.top||cG.y>(cF.top+cF.height)){b7()
}},bZ=function(cD){var cE=aa(cD);z.I.removeEvent(cE,"mousemove",bZ);if(!H.didMouseMove()){z.I.addEvent(cE,"mousemove",bZ);
return}while(cE.itemType===aL){cE=cE.parentNode}a2(cE)},cz=function(cD){bb=o&&bi.getInput()==document.activeElement;
return ae(cD)},bt=function(cD){var cE=aa(cD);while(cE.itemType===aL){cE=cE.parentNode
}switch(cE.itemType){case O:bJ();break;case u:cf();break;case S:bj(cE.transliterationIndex,az);
ci();break;case ac:H.setInstancesEnabled(true);cl();break;case U:H.setInstancesEnabled(false);
cl();break}},ct=function(){bk.setTransliterations(bR,cp,bw)},ci=function(){if(!a3()){return
}bA.style.display=aH;b3();if(by){H.reportTransliterationSelection(bR,cp.start,bi.getInput().value,s)
}H.adViewDec()},a3=function(){return bA.style.display!=aH},b7=function(){if(!bR){return
}var cD=bR.getSelectedIndex(),cE=bg.firstChild;do{if(cE.transliterationIndex===cD){a2(cE);
return}cE=cE.nextSibling}while(cE)},bj=function(cE,cG){if(!bi.validateWordSelection(cp)){return
}bR.setSelectedIndex(cE);if(cp.wordType==z.I.WordType.PURE_NUMBER){var cH=!cE;if(cH!=H.readGlobalPref(aD)){H.saveGlobalPref(aD,!cE)
}}var cI=bR.getSelectedTransliteration();if(cE!==0){H.registerArabicToRoman(cI,bm)
}var cF=bi.getInput();H.registerDefaultPick(cG&&cE==1&&cp.selectionEnd==cF.value.length);
bi.replaceInputValue(cI,cp.start,cp.end,az,az);var cD=cp.start+1;cp=i(cF.value,{start:cD,end:cD},s,bi.getOption("disableInMarkup"));
by=az},a2=function(cD){if(ch==cD){return}if(ch!==aL){ch.style.backgroundColor=ch.yamliMenuBackgroundColor;
ch.style.color=ch.yamliTextColor;if(ch.yamliChildBackground){ch.yamliChildBackground.style.backgroundColor=ch.style.backgroundColor
}}ch=cD;if(ch!==aL){ch.style.backgroundColor=ch.yamliMenuHighlightColor;ch.style.color=ch.yamliHighlightColor;
if(ch.yamliChildBackground){ch.yamliChildBackground.style.backgroundColor=ch.style.backgroundColor
}}},bJ=function(){var cF,cE;for(var cD=1;cD<bg.childNodes.length;++cD){cF=bg.childNodes[cD];
if(cF.itemType==S&&cF.style.display==aH){if(cE===aL){cE=cF;bj(cE.transliterationIndex,s)
}cF.style.display=ap}}b0.style.display=aH;bB.style.display=ap;if(cE===aL){cE=bB}return cE
},cf=function(){bj(0,s);by=s;ca()},ca=function(){bE();var cD="/not_found."+bi.getOption("uiLanguage")+".htm";
bg.appendChild(bC(H.makeUrl("/not_found.ashx?path="+aq(cD)+"&word="+aq(bm),"&"),az,bi.adjustAjaxWidth(160),bi.getSettingsMenuFontSize()));
aX=s},bx=function(){if(bi.getEnabled()){bD.style.display=aH;a1.style.display=ap;a(cm,bi.lang("quick_toggle_on"));
cm.title=bi.lang("quick_toggle_on_hint");if(b1){b1.style.backgroundPosition="left 0px"
}}else{a1.style.display=aH;bD.style.display=ap;a(cm,bi.lang("quick_toggle_off"));
cm.title=bi.lang("quick_toggle_off_hint");if(b1){b1.style.backgroundPosition="left -"+bO+"px"
}}if(cA){if(bi.getDirection()=="rtl"){bN.style.display=aH;cA.style.display=ap}else{cA.style.display=aH;
bN.style.display=ap}}if(bi.getOption("settingsPlacement")=="inside"){a0.style.display=aH
}else{a0.style.display=""}},cl=function(){if(cr.style.display==aH){return}cr.style.display=aH;
H.adViewDec()},bG=function(cD){ci();cy(true);if(cr.style.display==ap){cl()}else{cr.style.display=ap;
bS(cj,az)}ae(cD);bi.focusInput()},bP=function(cD){bi.focusInput();cy(true);H.setInstancesEnabled(!bi.getEnabled());
cl()},b2=function(cD){a2(aL)},cc=function(cD){cl()},be=function(cD){H.setInstancesEnabled(!bi.getEnabled());
cl();bi.focusInput()},cn=function(cD){bi.toggleDir();cl();bi.focusInput()},b9=function(cD){bK();
cl();bi.focusInput()},bT=function(cD){cw();bi.focusInput()},bp=function(){if(!bu){var cM=bi.getSettingsMenuFontSize(),cG=bi.adjustAjaxWidth(240);
bu=q(j);bu.style.width=cG+"px";a8.appendChild(bu);bu.className="yamliapi_menuBorder";
b6(a8);var cE=q(j);cE.className="yamliapi_menuPanel";bu.appendChild(cE);var cL=q(j);
cL.className="yamliapi_menuContent";cE.appendChild(cL);var cF=bi.lang("dir"),cJ=cu(aQ);
cJ.style.textAlign=cF=="rtl"?"right":"left";cL.appendChild(cJ);bd=new z.I.Draggable(cJ,bu);
var cH=q(j);cH.style.position=ag;if(cF=="ltr"){cH.style.right="0"}else{cH.style.left="0"
}cH.style.top="0";cH.style.padding=M;cJ.appendChild(cH);var cD=cv("close","tips_close_hint",bT);
cD.style.verticalAlign="top";cD.className+=" yamliNoDrag";cH.appendChild(cD);var cK=cC("tips_title");
cK.style.fontWeight="bold";cJ.appendChild(cK);var cI=a4();cL.appendChild(cI);cL.appendChild(bC("/tips."+bi.getOption("uiLanguage")+".htm",s,null,cM,bQ));
cg.push(cL);co.push(cD);bl()}},cd=function(){if(a8.style.display==ap){return}bp();
a8.style.display=ap;bQ()},cw=function(){if(a8.style.display==aH){return}a8.style.display=aH
},bK=function(){if(a8.style.display==aH){cd()}else{cw()}},bQ=function(){bW(a8,a9(bu,bF.x,s,s),a9(bu,bF.xr,az,s));
a8.style.top=(H.useRightLayout()?bF.yr:bF.y)+"px"},bq=function(){if(!bM){var cG=210,cH=bi.lang("dir"),cO=cH=="rtl";
bM=q(j);bM.style.width=cG+"px";cb.appendChild(bM);bM.style.border="#ddd solid 1px";
bM.style.margin="4px";bM.style.padding=cH=="ltr"?"3px 6px 3px 22px":"3px 22px 3px 6px";
bM.style.background="#FFFFDF url("+H.makeUrl("/cache_safe/bulb.gif","?")+") no-repeat top "+(cO?"right":"left");
b6(bM);var cD=q(j);cD.style.direction=cH;cD.style.textAlign=cO?"right":"left";bM.appendChild(cD);
cD.appendChild(aW("hint_content_start"));var cQ=q(j),cJ=H.makeUrl("/cache_safe/marhaban_movie_small.gif","?");
cQ.style.padding="10px 6px 0 6px";ab(cQ,cO?"left":"right");a(cQ,'<img style="border: 1px solid #888;height:60px" src="'+cJ+'" />');
cD.appendChild(cQ);var cL=q("div"),cM=bi.lang("hint_content_end"),cR=H.makeUrl("/cache_safe/logo_y_14.gif","?"),cP="14px",cN=H.makeUrl("/cache_safe/transparent_pixel.gif","?");
cL.style.paddingTop="1em";a(cL,cM.replace("[Y]",'<img src="'+cN+'" style="width:'+cP+";height:"+cP+";background:url('"+cR+"') no-repeat;background-position:left 0px\" />"));
cD.appendChild(cL);var cF=q(j);cF.style.paddingTop="0.5em";var cK=cv("hint_content_try","",b5);
cF.appendChild(cK);cD.appendChild(cF);cD.appendChild(bh());var cI=q(j);cD.appendChild(cI);
var cE=cv("hint_close","hint_close_hint",bH);cI.appendChild(cE);ce(cD,"direction",cH);
ce(cD,"textAlign",cO?"right":"left");ce(cD,"fontSize","11px");ce(cI,"textAlign",cO?"left":"right");
ce(cE,"fontSize","9px");ce(cK,"fontSize","13px")}},b5=function(){cy(true);if(bi.getOption("hintMode")=="startModeOff"){H.setInstancesEnabled(true)
}bK()},bH=function(){cy(true)},cy=function(cD){cb.style.display=aH;if(cD){H.setShowedHint()
}},cB=function(){var cE=bi.getDirection(),cH=bi.getInputDims(),cG=cb.offsetWidth,cD=cx.x,cF=cx.xr;
if(cE=="rtl"){cD+=cH.width-cG}else{cF+=cH.width-cG}bW(cb,a9(cb,cD,s,bi.useRtlMenu()),a9(cb,cF,az,bi.useRtlMenu()));
if(bi.isTextArea()){cb.style.top=(cH.top+1.5*bi.getInputFontSize())+"px"}else{cb.style.top=(cH.top+cH.height)+"px"
}},cq=function(){H.hideDefaultPickHint();ct()},a9=function(cF,cM,cG,cJ){if(!cF){return cM
}var cK=cF.style.display;if(cK==aH){cF.style.display=ap}var cI=cF.offsetWidth+2;if(cK==aH){cF.style.display=aH
}var cH=g;if(o){cH=(am&&am.clientWidth===0)?g:am}var cN=H.getClientWidth(),cE=r(),cL,cD;
if(cG){cD=cM-(cH.scrollWidth-cE-cN);cL=cN-cD-cI;if(cJ){cL-=cI;cD+=cI}if(cL<0){return cM+cL
}if(cD<0){return cM-cD}}else{cL=cM-cE;cD=cN-cL-cI;if(cJ){cL-=cI;cD+=cI}if(cL<0){return cM-cL
}if(cD<0){return cM+cD}}return cM},b3=function(){if(a6){clearTimeout(a6);a6=aL}};
this.show=function(){if(!a3()){bS(bf,az)}cl();bA.style.display=ap};this.hide=function(cD){ci();
if(cD){cl()}};this.isMenuVisible=a3;this.hideAndSelectCurrent=function(cE){var cD=ch;
if(!cp||!cD){return}switch(cD.itemType){case O:a2(bJ());break;case u:cf();break;case S:bj(cD.transliterationIndex,cE);
ci();break}};this.preloadAd=function(){bS(bf,s)};this.isVisible=function(){return bA.style.display==ap
};this.isVisibleAndNotLoading=function(){return bA.style.display==ap&&bR!=aL};this.isLoading=function(){return bR===aL&&this.isVisible()
};this.processUpKey=function(){if(!aX||!bR){return}var cD=ch;while(true){cD=cD.previousSibling;
if(!cD){cD=bg.lastChild;while(!cD.itemType){cD=cD.previousSibling}}if(cD.itemType!=h&&cD.itemType!=aI&&cD.style.display!=aH){break
}}switch(cD.itemType){case O:a2(cD);break;case u:a2(cD);break;case S:bj(cD.transliterationIndex,s);
a2(cD);break}};this.processDownKey=function(){if(!aX||!cp){return}var cD=ch;while(true){cD=cD.nextSibling;
if(!cD||!cD.itemType){cD=bg.firstChild}if(cD.itemType!=h&&cD.itemType!=aI&&cD.style.display!=aH){break
}}switch(cD.itemType){case O:a2(cD);break;case u:a2(cD);break;case S:bj(cD.transliterationIndex,s);
a2(cD);break}};this.setTransliterations=function(cY,cN,cW){bR=cY;cp=cN;bw=cW;b3();
var cE=[],cG=-1,cK;if(!cp||!bm||cp.word.substr(0,bm.length)!=bm){bc=H.getAdInfo()?4:2
}bm=cp.word;if(bR){cE=bR.getTransliterationsArray();cG=bR.getSelectedIndex();bm=cY.getRomanWord()
}bE();var cU,cP=cE.length,cM=cP>0?cE[0].type:0;if(H.showDefaultPickHint()&&cp.selectionEnd==bi.getInput().value.length){var cR=bi.lang("dir"),cL=a7(aI,bi.lang("def_pick_hint"),"#333","#FFFFDF","#333","#FFFFDF",cR,0.9);
cL.style.border="#ddd solid 1px";cL.style.margin="4px";cL.style.padding=cR=="ltr"?"3px 6px 3px 22px":"3px 22px 3px 6px";
cL.style.background="#FFFFDF url("+H.makeUrl("/cache_safe/bulb.gif","?")+") no-repeat top "+(cR=="ltr"?"left":"right");
var cZ=q(j);cZ.style.textAlign=cR=="ltr"?"right":"left";var cX=cv("close","",cq);
cX.style.fontSize="80%";cZ.appendChild(cX);cL.appendChild(cZ);bg.appendChild(cL)}var cQ=bm.replace("-","\u2011"),cI=a7(bR?S:aI,cQ,aB,R,bR?ax:aB,bR?Q:R,bi.useRtlMenu()?"rtl":"ltr");
cI.transliterationIndex=0;cI.style.fontStyle="italic";bg.appendChild(cI);if(cG===0){cK=cI
}if(cW&&cW<cP){cP=cW}for(cU=1;cU<cP;++cU){if(cU>=2){break}if(cE[cU].type>cM){if((cU+1)<cP){++cU
}break}}var cO=cU;if(cO==cP){cO--}var cV=az;if((cG-1)>cO||cO==(cP-1)||!cP){cV=s}for(cU=0;
cU<cP;++cU){var c0=a7(S,cE[cU].trans,aB,R,ax,Q,"rtl");c0.transliterationIndex=cU+1;
if(cV&&cU>cO){c0.style.display=aH}bg.appendChild(c0);if(cG===(cU+1)){cK=c0}}b0=a7(O,bi.lang("show_more"),ah,R,aP,Q,"ltr",0.8);
b0.style.textAlign="right";b0.style.display=aH;bg.appendChild(b0);bB=a7(u,bi.lang("report_word"),ah,R,aP,Q,"ltr",0.8);
bB.title=bi.lang("report_word_hint");bB.style.textAlign="right";bB.style.display=aH;
bg.appendChild(bB);var cF=0;if(cP>0){if(cV){b0.style.display=ap}else{bB.style.display=ap
}bc=Math.max(bc,Math.min(4,cP+1));cF=Math.max(0,bc-cP-1)}else{cF=bc}var cD;for(cU=0;
cU<cF;++cU){var cT=cU===0&&cP===0,cJ=cP===0&&cU==3,cH=(cT)?Q:R;var cS=a7(aI,cJ?"&nbsp;":"أ",cH,cH,cH,cH,"rtl",cJ?0.8:aL);
if(cT){cD=cS;cS.style.textAlign="right"}bg.appendChild(cS)}if(cD){a6=setTimeout(function(){var c3=cD.style.fontSize,c4=bi.getSettingsButtonSize(),c5=c4>18?"16x16":"12x12",c1=c4>18?'width="24" height="16"':'width="18" height="12"',c2=o?"أ":"&nbsp;";
c5="/cache_safe/spinner3_"+c5+".gif";a(cD,'<span style="vertical-align: middle;font-size:'+c3+";color:"+Q+';white-space:pre">'+c2+'<img style="visibility:hidden" '+c1+' src="'+H.makeUrl(c5,"?")+'" onload="this.style.visibility=\'visible\'" /></span>')
},400)}a2(cK);by=s;aX=az};this.updateFontSizes=bl;this.setMenuOffset=function(cD,cF,cG,cE){bW(bo,a9(bA,cx.x+cD,s,cE),a9(bA,cx.xr+cG,az,cE));
bo.style.top=(cx.y+cF)+"px";if(cE){bA.style.position=ag;bA.style.right="0";bA.style.left="auto"
}else{bA.style.position=ag;bA.style.left="0";bA.style.right="auto"}};this.setMainOffset=function(cF,cE,cD){cx.x=cF;
cx.y=cE;cx.xr=cD};this.setYamliSettingsPosition=function(cF,cE,cD){cs.x=cx.x+cF;cs.xr=cx.xr+cD;
cs.y=cx.y+cE;bW(br,cs.x,cs.xr);br.style.top=cs.y+"px"};this.hideYamliSettings=function(){br.style.visibility="hidden"
};this.showYamliSettings=function(){br.style.visibility="visible"};this.setYamliSettingsMenuPosition=function(cF,cE,cD){bY.x=cs.x+cF;
bY.xr=cs.xr+cD;bY.y=cs.y+cE;bW(cr,a9(cr,bY.x,s,s),a9(cr,bY.xr,az,s));cr.style.top=bY.y+"px";
cB()};this.setYamliTipsPosition=function(cG,cF,cD,cE){bF.x=cx.x+cG;bF.xr=cx.xr+cD;
bF.y=cx.y+cF;bF.yr=cx.y+cE;bQ()};this.getYamliSettingsDims=function(){return z.I.getDimensions(br)
};this.hasSelectionChanged=function(){return by};this.checkCancelBlur=function(){var cD=bb;
bb=s;return cD};this.getWordSelection=function(){return cp};this.hideSettings=cl;
this.resetSettingsUI=bx;this.activateAds=function(){for(var cD in bv){if(bv.hasOwnProperty(cD)){bU(bv[cD])
}}bv=aL};this.showHint=function(){bq();cb.style.display=ap;cB()};this.hideHint=cy;
this.isEltDescendant=function(cD){return ao(cD,bo)};this.unload=function(){b3();z.I.removeEvent(bg,"mouseout",aZ);
z.I.removeEvent(bz,"mouseout",b2);H.removeChild(bo);bo=bA=b4=bg=bf=bf.ad=b0=bB=aL;
z.I.removeEvent(ck,"dblclick",be);H.removeChild(br);ck=br=a0=cj=cj.ad=cm=aL;H.removeChild(cr);
cr=bX=bz=b1=bD=a1=bN=cA=aL;if(bd){bd.unload()}H.removeChild(a8);a8=bu=co=cg=bd=aL;
H.removeChild(cb);cb=bM=aL;ch=cp=bi=bR=bm=aL;m(a5);a5=aL};bn()},an=function(a0,aX){var aZ=a0,aY=aX,aW=1;
this.getTransliterationsArray=function(){return aY};this.getRomanWord=function(){return aZ
};this.getSelectedIndex=function(){return aW};this.setSelectedIndex=function(a1){aW=a1
};this.setSelectedWord=function(a2){for(var a1=0;a1<aY.length;++a1){if(aY[a1].trans==a2){aW=a1+1;
return}}};this.getTransliterationCount=function(){return aY.length};this.getSelectedTransliteration=function(){if(aY.length===0){return aL
}if(aW===0){return aZ}return aY[aW-1].trans}},E=function(){var bC,aW={},bj={},bT=[],a1=[],bz=[],bO="http://",bV="api.yamli.com";
var bd=[],bR=s,bc=s,bg,bs=[],bo=[],bt,aY,bS,bp,bb,bn=s,aX=aL,bA=s,bB=s,bG=s,bE=0,bx,bN,bm=new aO(),a6=0,by=0,bD=s,bP=s,bJ=s,bw={x:-1,y:-1},bK=0,a0=0,bk={},a3,bf={},bi=function(bZ,bY){var bX=H.createTransliterationRequest(bZ);
bX.onreadystatechange=function(){var b0=H.processTransliterationResponse(bX);if(b0==1){var b1=aU(bZ),b2=H.getTransliterations(b1,bZ);
b2.setSelectedWord(bY);H.registerArabicToRoman(bY,bZ)}};bX.send()},br=function(){return bg!==aL
},bv=function(b3){g=document.body;try{bg=Yamlieval2(b3)}catch(b2){bg=aL}if(!bg){return
}if(bg.authorization=="staleClient"){bL(true,bg.serverBuild);return}if(bg.authorization=="userBlacklisted"){if(document.location.hostname.toLowerCase().indexOf("yamli.com")!=-1){alert("Your IP address has been blacklisted due to violations to the Yamli Terms of Service (http://www.yamli.com/terms/).\n\nPlease contact Yamli at info@yamli.com for more information.")
}return}if(bg.authorization!="authorized"){return}bf=bg.prefs?bg.prefs:bf;var b5=bg.options,b1,bZ;
for(var bX in b5){if(b5.hasOwnProperty(bX)){bS[bX]=b5[bX];for(b1 in aW){if(aW.hasOwnProperty(b1)){aW[b1][bX]=b5[bX]
}}}}var b4=J(g);if(b4.direction=="rtl"){if(o){bn=az}}var bY=bW("zIndexBase");bN=aM(".yamliapi_parent {width: 100%;}\n.yamliapi_parent *{line-height:normal;font-size:"+aw+";font-style:normal;font-weight:normal;font-family:"+bS.uiFontFamily+";direction:ltr;color:black;letter-spacing:normal;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:normal;padding:0;margin:0;border-width:0;}\n.yamliapi_settingsDiv,.yamliapi_settingsDiv *{z-index:"+(bY+1)+";white-space:nowrap;}\na.yamliapi_simpleLink, a.yamliapi_simpleLink *{color:"+al+";white-space:nowrap;cursor:pointer;}\na.yamliapi_simpleLink:hover, a.yamliapi_simpleLink:hover *{text-decoration:underline!important;background-color:transparent!important;}\n.yamliapi_button {border: solid 1px #dde4ee;background-color: #4d679a!important;cursor:pointer;font-weight:bold;padding:1px 0.3em;color: white;text-decoration: none;text-align: center;white-space:nowrap;display:inline;}\n.yamliapi_anchor, .yamliapi_anchor *,.yamliapi_anchor:link, .yamliapi_anchor:link *,.yamliapi_anchor:visited, .yamliapi_anchor:visited *,.yamliapi_anchor:hover, .yamliapi_anchor:hover *,.yamliapi_anchor:active, .yamliapi_anchor:active *{color:"+al+"!important;background-color:transparent!important;text-decoration:none!important;white-space:nowrap!important;cursor:pointer!important;}\n.yamliapi_anchor_y {color:"+al+"!important;font-weight: bold; }\n.yamliapi_anchor:hover, .yamliapi_anchor:hover *{text-decoration:underline!important;}\n.yamliapi_menuBorder {border:1px solid #a0a0a0;}\n.yamliapi_menuPanel {padding:1px;background-color:#e0e0e0;}\n.yamliapi_menuContent {padding:1px;background-color:"+R+";}\n.yamliapi_rtlContent, yamliapi_rtlContent *{direction:rtl;text-align:right;}\n.yamliapi_clear {clear: both;height: 1px;line-height: 1px;font-size: 1px;}\niframe.yamli_iframeselectfix {position:absolute;top:0;z-index:-1;filter:mask();width:400px;height:800px;}\n");
bt=q(j);bt.id="yamli_div";bt.className="yamliapi_parent";bt.style.position=ag;bt.style.top="0";
bt.style.overflow="visible";bt.style.zIndex=bY;a(bt,'<div id="yamliapi_dyn_div" style="position:absolute;top:0;width:100%"></div>');
g.appendChild(bt);aY=n("yamliapi_dyn_div");bp=q(j);bp.style.fontSize="10px";bp.style.position=ag;
bp.style.visibility="hidden";a(bp,"Yamli rocks");bp.style.top="0";if(bn){bp.style.right=aY.style.right=bt.style.right="0"
}else{bp.style.left=aY.style.left=bt.style.left="0"}bt.appendChild(bp);bb=aT(J(bp).fontSize)/10;
bq();bB=a9("yamli_showed_hint")!="true"&&bg.showHint;if(bB){aZ()}bG=bg.showPowered;
for(b1 in aW){if(aW.hasOwnProperty(b1)){bI(b1,aW[b1])}}for(bZ in bj){if(bj.hasOwnProperty(bZ)){bU(bZ,bj[bZ])
}}bx=setInterval(a4,1000);z.I.addEvent(window,"resize",ba);z.I.addEvent(window,"load",ba);
z.I.addEvent(document,"mousewheel",bh);z.I.addEvent(document,"DOMMouseScroll",bh);
z.I.addEvent(window,"unload",bQ);for(var b0=0;b0<bs.length;++b0){bs[b0]()}bs=aL},ba=function(){for(var bX=0;
bX<bd.length;++bX){bd[bX].resetLayout()}},a7=function(bY){var bX=aj(bY);if(bX.x!=bw.x||bX.y!=bw.y){bJ=az;
bw=bX}},bh=function(){for(var bX=0;bX<bd.length;++bX){bd[bX].hideTransliterations()
}},bQ=function(){z.I.removeEvent(window,"unload",bQ);for(var bX=0;bX<bo.length;++bX){bo[bX]()
}H.resetYamli(false)},aZ=function(){var bX=q("img");bX.width=bX.height=0+"px";z.I.addEvent(bX,"load",function(){aY.removeChild(bX);
bX=aL});aY.appendChild(bX);bX.src=H.makeUrl("/cache_safe/marhaban_movie_small.gif","?")
},bq=function(){var bY=bg.adInfo;if(bY){switch(bY.adType){case"image":aX=q("img");
break;case"iframe":aX=q("iframe");aX.frameBorder="0";aX.scrolling="no";aX.src="about:blank";
break;case"gam":bY.url=bF(bY.url);aX=q("iframe");aX.frameBorder="0";aX.scrolling="no";
break}if(aX){aX.width=aX.height=0+"px";z.I.addEvent(aX,"load",bM);var bX=aX;aY.appendChild(aX);
bX.src=bY.url}}},bM=function(){if(aX){z.I.removeEvent(aX,"load",bM);aY.removeChild(aX);
aX=aL}bA=az;for(var bX=0;bX<bd.length;++bX){bd[bX].activateAds()}},bW=function(bX){return bS[bX]
},bF=function(b0,bX){var bY=bO+bV+b0;if(bX){bY+=bX+"build="+z.I.buildNumber}var bZ=bW("extraHttpParams");
if(bZ.length>0){bY+=(bY.indexOf("?")==-1?"?":"&")+bZ}return bY},a2=function(bX,bY){z.I.setCookie(bX,bY,1825)
},a9=function(bX){return z.I.getCookie(bX)},bl=function(bY,b0){bf[bY]=b0;var bX=H.makeUrl("/set_preferences.ashx?"+aq("apipref_"+bY)+"="+aq(b0),"&");
var bZ=z.I.createSXHR();bZ.open(bX);bZ.send()},bH=function(bX){return bf[bX]},be=function(){var bY=0,b2,b0;
for(var b3 in bk){if(bk.hasOwnProperty(b3)){++bY;try{if(window.frames[b3].frames.length==2){b0=window.frames[b3].frames[1];
b2=b0.document.location.hash;if(b2=="#stop"){delete bk[b3];--bY;if(B){b0.document.location.replace("about:blank")
}}else{if(b2.length>1){var bX=b2.substr(1).split(","),bZ=document.getElementById(b3);
bZ.parentNode.style.width=bX[0]+"px";bZ.width=bX[0]+"px";bZ.height=bX[1]+"px";delete bk[b3];
--bY;if(B){b0.document.location.replace("about:blank")}}}}}catch(b1){}}}if(!bY){clearInterval(a3);
a3=aL}},a4=function(){if(bt.parentNode!=g){g.appendChild(bt)}var bX=aT(J(bp).fontSize)/10,b0=r(),bZ=bX!=bb||b0!=bE;
bb=bX;bE=b0;for(var bY=0;bY<bd.length;++bY){bd[bY].resetLayout(bZ)}},a5=function(){a8()
},a8=function(){if(bS.reportErrors){z.setupRuntimeErrorHandling()}var bX=H.makeUrl("/checkin.ashx"+H.getReferrerInfo("?"),"&"),bY=z.I.createSXHR();
bY.open(bX);bY.onreadystatechange=function(){bv(bY.responseText)};bY.send()},bI=function(b0,bZ){var bY=C(b0);
var bX=bu(bY,bZ);if(bX){bX.yamlifyId=b0}},bU=function(bX,b5){var b3=[],bZ,bY,b1=aA("textarea"),b0=aA("input"),b2=new RegExp("(^|\\s)"+bX.replace(/\-/g,"\\-")+"(\\s|$)"),b4;
for(bZ=0;bZ<b1.length;++bZ){bY=b1[bZ];if(b2.test(bY.className)){b3.push(bY)}}for(bZ=0;
bZ<b0.length;++bZ){bY=b0[bZ];if(bY.type=="text"&&b2.test(bY.className)){b3.push(bY)
}}for(bZ=0;bZ<b3.length;++bZ){bY=b3[bZ];b4=bu(bY,b5);if(b4){b4.yamlifyClass=bX}}},bu=function(bY,bZ){if(!bY||typeof bY!="object"||(bY.type!="text"&&bY.type!="textarea")){return
}if(bY.yamliManager!==aL){return}var b3=new ak(bY,bZ);bY.yamliManager=b3;bd.push(b3);
var b2;if(bZ.formOverride){b2=C(bZ.formOverride)}else{var bX=bY;while(true){bX=bX.parentNode;
if(!bX){break}if(bX.tagName&&bX.tagName.toLowerCase()=="form"){b2=bX;break}}}if(b2){b3.setForm(b2);
var b1,b4=s;for(var b0=0;b0<b2.length;++b0){if(b2[b0]==bY){b4=az}else{if(b2[b0].type=="submit"||b2[b0].type=="image"){b1=b2[b0]
}}}if(!b4){b1=aL}b3.setSubmitButton(b1)}return b3},bL=function(bX,b6){if(b6){z.debug("detected old Yamli client, client build is: "+z.I.buildNumber+", server build is : "+b6)
}clearInterval(bx);bx=aL;if(a3){clearInterval(a3);a3=aL}z.I.SXHRData.unload();z.I.SXHRData=aL;
z.I.removeEvent(window,"resize",ba);z.I.removeEvent(window,"load",ba);z.I.removeEvent(document,"mousewheel",bh);
z.I.removeEvent(document,"DOMMouseScroll",bh);z.I.removeEvent(window,"unload",bQ);
for(var b2=0;b2<bd.length;++b2){bd[b2].unload()}bd=aL;bT=a1=bz=bs=bo=aL;if(bt){g.removeChild(bt)
}bt=aY=bp=aL;if(aX){z.I.removeEvent(aX,"load",bM)}aX=aL;if(bN){m(bN)}z.domReady.unload();
z.domReady=aL;var b3=bS,bZ=aW,b4=bj;bS=aW=bj=aL;b3.assumeDomReady=az;var b1="yamli_reload_script";
if(n(b1)){window.Yamli=z=aL;return}var bY;if(bX){bY=q("script");bY.id=b1;bY.src="http://api.yamli.com/js/yamli_api.js?"+b6;
var b5=function(){var b7=window.Yamli;if(typeof(b7)=="object"&&b7.init(b3)){for(var b9 in bZ){if(bZ.hasOwnProperty(b9)){b7.yamlify(b9,bZ[b9])
}}for(var b8 in b4){if(b4.hasOwnProperty(b8)){b7.yamlifyClass(b8,b4[b8])}}}b3=b4=bZ=aL
};switch(l){case t:case aE:bY.onload=b5;break;case aK:var b0=0;bY.onreadystatechange=function(){if(bY.readyState=="loaded"){++b0;
if(b0==2){bY.onreadystatechange=null;b5()}}};break;default:bY.onreadystatechange=function(){if(bY.readyState=="loaded"||bY.readyState=="complete"){bY.onreadystatechange=null;
b5()}}}}window.Yamli=z=aL;if(bY){aA("head")[0].appendChild(bY)}};this.sendOneWay=function(bX){var bY=z.I.createSXHR();
bY.open(bX);bY.onreadystatechange=function(){};bY.send()};this.getOption=bW;this.readGlobalPref=bH;
this.saveGlobalPref=bl;this.registerTransliteration=function(b0,bZ,bX){var bY=new an(b0,bZ);
bT["_"+b0]=bY;if(bX!==aL){bY.setSelectedIndex(bX)}};this.getTransliterations=function(bX,bY){switch(bX){case z.I.WordType.PURE_ROMAN:case z.I.WordType.PURE_NUMBER:case z.I.WordType.ROMAN_ARABIC:return bT["_"+bY];
case z.I.WordType.PURE_ARABIC:var bZ=a1["_"+bY];if(bZ===aL){return aL}return bT["_"+bZ]
}return aL};this.getTransliterationsFromWordSelection=function(bX){return this.getTransliterations(bX.wordType,bX.word)
};this.registerArabicToRoman=function(bX,bZ){var bY="_"+bX;a1[bY]=bZ;delete bz[bY]
};this.registerDashedWord=function(b0){for(var bZ=0;bZ<(b0.length-1);++bZ){for(var bY=bZ+2;
bY<=b0.length;++bY){var bX=b0.slice(bZ,bY);bz["_"+bX.join("")]=bX}}};this.registerDefaultPick=function(bX){bK=bX?++bK:0
};this.hideDefaultPickHint=function(){bl("hide_default_pick_hint","1")};this.showDefaultPickHint=function(){return bK>1&&bH("hide_default_pick_hint")!=1
};this.isRegisteredDashedWord=function(bX){return bz["_"+bX]!==aL};this.getDashedWordArray=function(bX){return bz["_"+bX]
};this.convertDashedWordSelection=function(bZ){var b2=bz["_"+bZ.word];if(b2===aL){return bZ
}var bX=-1,b1=0;do{++bX;b1+=b2[bX].length}while(b1<bZ.selectionOffset&&bX<(b2.length-1));
var b0=b2[bX],b4=bZ.start+b1-b0.length,bY=bZ.start+b1,b3=bZ.selectionOffset+b0.length-b1,b5=aU(b0);
return{word:b0,start:b4,end:bY,selectionOffset:b3,selectionStart:bZ.selectionStart,selectionEnd:bZ.selectionEnd,isEdge:(b3===0||b3===b0.length),wordType:b5}
};this.createTransliterationRequest=function(bZ){var bY=z.I.createSXHR(),bX=bF("/transliterate.ashx?word="+aq(bZ)+H.getReferrerInfo("&"),"&");
bY.open(bX);bY.onreadystatechange=function(){H.processTransliterationResponse(bY)
};return bY};this.processTransliterationResponse=function(b0){if(b0.readyState!=4){return -1
}if(b0.status!=200){return 0}var b2=Yamlieval2(b0.responseText);if(b2.staleClient){H.resetYamli(true,b2.serverBuild);
return}var b3=b2.w,b1=b2.r.split("|");if(b1.length===1&&b1[0].length===0){b1=[]}var bZ=[];
for(var bX=0;bX<b1.length;++bX){var bY=b1[bX].split("/");bZ.push({trans:bY[0],type:bY[1]})
}this.registerTransliteration(b3,bZ);return 1};this.savePrecache=function(bY){var b3=bY.split(z.I.HasPunctuationRegexp),b1=["1"];
for(var bZ=0;bZ<b3.length;++bZ){var b2=b3[bZ],bX=aU(b2),b0=H.getTransliterations(bX,b2);
if(b0===aL){continue}var b4=b0.getRomanWord();if(b2==b4){continue}b1.push(b4+"|"+b2)
}if(b1.length==1){return aL}return aq(b1.join("/"))};this.loadPrecache=function(b1){if(!b1||b1.length===0){return
}var b2=decodeURIComponent(b1).split("/"),bX=b2[0];if(bX<1){return}for(var b0=1;b0<b2.length;
++b0){var bY=b2[b0].split("|"),b3=bY[0],bZ=bY[1];bi(b3,bZ)}};this.setInstancesEnabled=function(bX){for(var bY=0;
bY<bd.length;++bY){bd[bY].setEnabled(bX)}a2("yamli_startMode",bX?"on":"off")};this.getReferrerInfo=function(bY){var bX=document.location;
return bY+"tool="+aq(bS.tool)+"&account_id="+bS.accountId+"&prot="+aq(bX.protocol)+"&hostname="+aq(bX.hostname)+"&path="+aq(bX.pathname)
};this.isAdLoaded=function(){return bA};this.getAdIframeId=function(){var bX="yamli_spid_"+a0;
++a0;bk[bX]=1;if(!a3){a3=setInterval(be,100)}return bX};this.debugAds=au().yamli_debugAds;
this.debugAds=this.debugAds===aL?"":this.debugAds;this.shouldEnableOnStart=function(bY){switch(bY){case"on":return az;
case"off":return s}var bX=a9("yamli_startMode");switch(bX){case"on":return az;case"off":return s
}return(bY=="offOrUserDefault")?s:az};this.getAdInfo=function(){return bg.adInfo};
this.reportTyped=function(){if(bc){return}bc=az;var bX=H.makeUrl("/report_typed.ashx"+H.getReferrerInfo("?"),"&");
this.sendOneWay(bX)};this.reportUsed=function(){if(bR){return}bR=az;var bX=H.makeUrl("/report_used.ashx"+H.getReferrerInfo("?"),"&");
this.sendOneWay(bX)};this.reportImpression=function(){if(bD){return}bD=az;var bZ=H.getAdInfo(),bY=bZ?bZ.adId:"NONE",bX=H.makeUrl("/report_impression.ashx?sp_id="+aq(bY)+H.getReferrerInfo("&"),"&");
this.sendOneWay(bX)};this.reportImpressionTime=function(){if(bP){return}bP=az;var b0=H.getAdViewElapsed();
if(b0===0){return}var bZ=H.getAdInfo(),bY=bZ?bZ.adId:"NONE",bX=H.makeUrl("/report_impression_time.ashx?sp_id="+aq(bY)+"&viewed_ms="+b0+"&click_count="+by+H.getReferrerInfo("&"),"&");
this.sendOneWay(bX)};this.reportTransliterationSelection=function(bX,b6,b2,b3){var b7=bX.getSelectedTransliteration();
if(b7===aL){return}var bZ=bX.getRomanWord(),b5="",b0=0,b4;for(var b1=b6-1;b1>=0;--b1){b4=b2.charAt(b1);
if(ar.hasOwnProperty(b4)){if(b4!=" "&&b4!="-"){break}++b0;if(b0>6){break}}b5=b4+b5
}var bY=H.makeUrl("/report_selection.ashx?roman_word="+aq(bZ)+"&selection="+aq(b7)+"&auto="+(b3?"true":"false")+"&prevText="+aq(b5)+H.getReferrerInfo("&"),"&");
this.sendOneWay(bY);this.reportUsed()};this.setShowedHint=function(){if(!bB){return
}bB=s;a2("yamli_showed_hint","true")};this.getShowHint=function(){return bB};this.getShowPowered=function(){return bG
};this.appendChild=function(bX){aY.appendChild(bX)};this.removeChild=function(bX){aY.removeChild(bX)
};this.appendVisChild=function(bX){bt.appendChild(bX)};this.removeVisChild=function(bX){bt.removeChild(bX)
};this.makeUrl=function(bY,bX){return bF(bY,bX)};this.useRightLayout=function(){return bn
};this.fixTextZoom=function(bX){return(bX/bb)};this.getClientWidth=function(){if(typeof window.innerWidth=="number"){return bt.clientWidth
}else{if(am&&(am.clientWidth||am.clientHeight)){return am.clientWidth}}return g.clientWidth
};this.adViewInc=function(){if(a6===0){bm.start();z.I.addEvent(g,"mousemove",a7)}a6++;
H.reportImpression()};this.adViewDec=function(){a6--;if(a6===0){bm.stop();z.I.removeEvent(g,"mousemove",a7)
}};this.getAdViewElapsed=function(){var bX=bm.getElapsed();return bX};this.adClickCountInc=function(){++by
};this.addCheckinCallback=function(bX){if(bg){bX();return}bs.push(bX)};this.addWindowUnloadCallback=function(bX){bo.push(bX)
};this.didMouseMove=function(){return bJ};this.clearMouseMoved=function(){bJ=s};this.isInitialized=function(){return bC===az
};this.setInitParams=function(bX,bY){if(bC!==aL){return bC}bO="http://";this.setGlobalOptions(bX,az,s);
aS();if(l==D){if(bY){var bZ=n(bY);if(bZ){a(bZ,aF);bZ.style.display=ap}}bC=s;return bC
}z.domReady=new z.DomReady();if(bS.assumeDomReady){a5()}else{z.domReady.addCallback(a5)
}bC=az;return bC};this.setGlobalOptions=function(bY,b0,b2){if(bY===aL){bY={}}if(b0){bS={}
}var bX={accountId:"",assumeDomReady:s,disableInMarkup:az,extraHttpParams:"",generateOnChangeEvent:s,hintMode:"startModeOff",insidePlacementPadding:0,maxResults:aL,uiFontFamily:"tahoma,sans-serif",uiLanguage:"en",reportErrors:s,settingsColor:"black",settingsLinkColor:al,settingsFontSize:"auto",settingsPlacement:"bottomRight",settingsXOffset:0,settingsYOffset:0,showDirectionLink:az,showTutorialLink:az,startMode:"onOrUserDefault",tool:"api",tutorialUrl:aL,zIndexBase:1000};
for(var b1 in bX){if(bX.hasOwnProperty(b1)){if(bY.hasOwnProperty(b1)&&typeof bY[b1]!="undefined"){bS[b1]=bY[b1]
}else{if(!bS.hasOwnProperty(b1)){bS[b1]=bX[b1]}}}}if(b2&&br()){for(var bZ in aW){if(aW.hasOwnProperty(bZ)){this.removeInstanceById(bZ,s);
bI(bZ,aW[bZ])}}}};this.setupInstancesById=function(b0,bX,b1){if(b1){aW[b0]={formOverride:aL}
}else{if(!aW.hasOwnProperty(b0)){return}}var bZ=aW[b0];if(bX===aL){bX={}}for(var bY in bX){if(bX.hasOwnProperty(bY)){bZ[bY]=bX[bY]
}}this.removeInstanceById(b0,s);if(br()){bI(b0,bZ)}};this.removeInstanceById=function(bY,b0){var bX=0,bZ;
while(bX<bd.length){bZ=bd[bX];if(bZ.yamlifyId==bY){bZ.unload();G(bd,bX)}else{++bX
}}if(b0&&aW.hasOwnProperty(bY)){delete aW[bY]}};this.setupInstancesByClass=function(b0,bX,b1){if(b1){bj[b0]={formOverride:aL}
}else{if(!bj.hasOwnProperty(b0)){return}}var bZ=bj[b0];if(bX===aL){bX={}}for(var bY in bX){if(bX.hasOwnProperty(bY)){bZ[bY]=bX[bY]
}}this.removeInstancesByClass(b0,s);if(br()){bU(b0,bZ)}};this.removeInstancesByClass=function(bY,b0){var bX=0,bZ;
while(bX<bd.length){bZ=bd[bX];if(bZ.yamlifyClass==bY){bZ.unload();G(bd,bX)}else{++bX
}}if(b0&&bj.hasOwnProperty(bY)){delete bj[bY]}};this.resetYamli=bL},ak=function(cd,bS){var b9=cd,bj=s,bu,be,a7,a9=[],bb,a4,cu={x:0,y:0},ck=!o,bt=0,bk=this,bp,aZ,br,bE,a3,bW,bi,ct,cv,cx,bV,ci,bJ,bO={fontFamily:"",fontStyle:"",fontWeight:"",paddingLeft:"",paddingRight:"",paddingTop:"",paddingBottom:"",borderLeftWidth:"",borderRightWidth:"",borderTopWidth:"",borderBottomWidth:"",fontSize:"",direction:""},aX,cr,bD,b1,by,bU,bX,bf=1,cg=s,bR=s,a0=s,ce=s,bM=s,cj=s,bG=az,cq,bI,bq,co=bS,b0=s,cs=-1,cw=b9.ownerDocument,bn=function(){bj=b9.type=="textarea";
aX=b9.getAttribute("autocomplete");var cz=J(b9);bJ=cz.direction;b9.dir=bJ;b9.style.direction=bJ;
cb();if(navigator.userAgent.indexOf("Firefox")!=-1){bt=ca("MMMMMMMMNNNN")}a4=new d(bk);
bd(true);if(ck){bh({x:bu.width,y:bu.height})}bl();z.I.addEvent(b9,"blur",bz);z.I.addEvent(b9,"keydown",bP);
z.I.addEvent(b9,"keyup",aW);z.I.addEvent(b9,"keypress",aY);z.I.addEvent(b9,"click",cp);
z.I.addEvent(b9,"mousedown",a8);z.I.addEvent(b9,"focus",b3);bQ(H.shouldEnableOnStart(bv("startMode")));
bd(true)},bQ=function(cz){cj=cz;var cA=b9.value.split(V).length<=1;if(!cj){a4.hide(true);
if(cA){b8(bJ)}}else{if(cA){b8("rtl")}}if(!bj){b9.setAttribute("autocomplete",cj?"off":aX)
}bd(true)},b2=function(){z.I.removeEvent(b9,"blur",bz);z.I.removeEvent(b9,"keydown",bP);
z.I.removeEvent(b9,"keyup",aW);z.I.removeEvent(b9,"keypress",aY);z.I.removeEvent(b9,"click",cp);
z.I.removeEvent(b9,"mousedown",a8);z.I.removeEvent(b9,"focus",b3);if(bI){z.I.removeEvent(bI,"submit",bB)
}if(bq){z.I.removeEvent(bq,"blur",bs);z.I.removeEvent(bq,"focus",bH);z.I.removeEvent(bq,"mousedown",bx)
}b7();a4.unload();var cA=function(){};if(a7){a7.onreadystatechange=cA}for(var cz=0;
cz<a9.length;++cz){a9[cz].onreadystatechange=cA}a9=aL;b9.yamliManager=aL;b9=bu=a4=bk=bI=bq=bO=cw=aL
},cb=function(cD){var cF=J(b9);if(!cD&&bO.fontFamily===cF.fontFamily&&bO.fontStyle===cF.fontStyle&&bO.fontWeight===cF.fontWeight&&bO.lineHeight===cF.lineHeight&&bO.paddingLeft===cF.paddingLeft&&bO.paddingRight===cF.paddingRight&&bO.paddingTop===cF.paddingTop&&bO.paddingBottom===cF.paddingBottom&&bO.borderLeftWidth===cF.borderLeftWidth&&bO.borderRightWidth===cF.borderRightWidth&&bO.borderTopWidth===cF.borderTopWidth&&bO.borderBottomWidth===cF.borderBottomWidth&&bO.fontSize===cF.fontSize&&bO.direction===cF.direction){return s
}bO.fontFamily=cF.fontFamily;bO.fontStyle=cF.fontStyle;bO.fontWeight=cF.fontWeight;
bO.direction=cF.direction;bO.lineHeight=cF.lineHeight;br=bF(bO.paddingLeft=cF.paddingLeft);
bE=bF(bO.paddingRight=cF.paddingRight);bi=bF(bO.paddingTop=cF.paddingTop);ct=bF(bO.paddingBottom=cF.paddingBottom);
cv=bF(bO.borderLeftWidth=cF.borderLeftWidth);cx=bF(bO.borderRightWidth=cF.borderRightWidth);
bV=bF(bO.borderTopWidth=cF.borderTopWidth);ci=bF(bO.borderBottomWidth=cF.borderBottomWidth);
aZ=H.fixTextZoom(bF(bO.fontSize=cF.fontSize,az));if(a3===aL){a3=br;bW=bE}cr=aZ;b1=bv("settingsFontSize");
var cA=b1;if(b1=="auto"){var cG=Math.max(aZ,9);cA=cG+"px";if(cG>=24){cG-=6}else{if(cG>=20){cG-=4
}else{if(cG>=18){cG-=3}else{if(cG>=16){cG-=2}else{if(cG>=14){cG-=1}}}}}b1=cG+"px"
}var cB=W(b1);bD=b1;bX=Math.max(9,parseFloat(cB.value,10)*0.7)+cB.unit;var cC=q(j);
a(cC,"AM(LZ");cC.style.position=ag;cC.style.top="0";cC.style.left="0";cC.style.fontFamily=bO.fontFamily;
cC.style.fontSize=cA;H.appendVisChild(cC);var cz=cC.offsetHeight-2,cE=parseInt(cz/2,10)*2;
by=0;bU=0;if(bv("settingsPlacement")!="hide"){by=Math.min(Math.max(cE,14),32);bU=Math.floor((cE-by)/2)
}cC.style.fontSize=bD;cC.style.fontFamily=bv("uiFontFamily");bf=cC.offsetHeight/12;
H.removeVisChild(cC);if(a4){a4.updateFontSizes()}return az},bF=function(cE,cB){if(cE.indexOf("px")!=-1){return parseInt(W(cE).value,10)
}if(cB){if(bj){var cA=g.createTextRange();cA.moveToElementText(b9);cA.collapse();
return cA.boundingHeight-3}else{return b9.clientHeight-bi-ct-3}}var cD=b9.style.left,cC=b9.runtimeStyle.left;
b9.runtimeStyle.left=b9.currentStyle.left;b9.style.left=cE||0;var cz=b9.style.pixelLeft;
b9.style.left=cD;b9.runtimeStyle.left=cC;return cz},ca=function(cB){var cz=q(j);cz.style.position=ag;
cz.style.visibility="hidden";cz.style.fontFamily=bO.fontFamily;cz.style.fontSize=aZ+"px";
cz.style.fontStyle=bO.fontStyle;cz.style.fontWeight=bO.fontWeight;a(cz,cB.replace(/ /g,"&nbsp;"));
H.appendChild(cz);var cA=cz.offsetWidth;H.removeChild(cz);return cA},bc=function(cK,cA){var cM={x:0,y:0},cJ=cm(),cH=cJ=="rtl",cD=n("yamli_temp");
if(cD){H.removeChild(cD)}var cz=br+cv,cB=bi+bV;if(cH&&(aR||(c&&K<3))){cz+=cy()}if(cH&&v&&cy()===0){cz+=17
}if(w){cz+=3}cD=q(j);cD.id="yamli_temp";cD.style.top=(bu.top+cB)+"px";cD.style.left=(bu.left+cz)+"px";
var cG=q("pre");cD.appendChild(cG);cD.style.position=ag;cD.style.visibility="hidden";
cG.style.margin="0";cG.style.border="0";cG.style.padding="0";cG.style.lineHeight=bO.lineHeight;
cG.style.whiteSpace="pre-wrap";if(c&&K<=3&&at===0){cG.style.whiteSpace="-moz-pre-wrap"
}cG.style.fontFamily=bO.fontFamily;cG.style.fontSize=aZ+"px";cG.style.fontStyle=bO.fontStyle;
cG.style.fontWeight=bO.fontWeight;cG.style.direction=cJ;cG.style.textAlign=cJ=="rtl"?"right":"left";
var cC=cD.style.width,cI=bL();cD.style.width=cI+"px";var cE=cw.createTextNode(cK.substr(0,cA.start)),cF=q("span"),cL=cw.createTextNode(cK.substr(cA.end,100));
cF.setAttribute("style",cG.getAttribute("style"));a(cF,cK.substring(cA.start,cA.end));
cG.appendChild(cE);cG.appendChild(cF);cG.appendChild(cL);H.appendChild(cD);cM.x=cF.offsetLeft+cz;
cM.y=cF.offsetTop+cF.offsetHeight-b9.scrollTop+cB+1;H.removeChild(cD);return cM},cy=function(){if(!bj){return 0
}var cz=b9.offsetWidth-b9.scrollWidth-cv-cx;if(!o&&!v){cz-=br+bE}return cz},bL=function(){var cz;
switch(l){case t:cz=b9.scrollWidth;if(K>=3){cz-=2}break;case aK:cz=b9.scrollWidth;
break;case aE:cz=b9.scrollWidth-6;if(v){cz=b9.scrollWidth-br-bE-17+cs}break;default:cz=b9.scrollWidth-br-bE;
break}return cz},bd=function(cC){var cO=be&&bu&&bu.width===0;be=p(b9);if(be){if(cb(cO)){cC=az
}var cP=cy();if(cs!=cP){cs=cP;cC=az}var cB=z.I.getDimensions(b9);if(!cC&&bu&&cB.left==bu.left&&cB.width==bu.width&&cB.top==bu.top&&cB.height==bu.height){return
}bu=cB}if(!be||bu.width===0){a4.hideYamliSettings();return}a4.resetSettingsUI();var cz=a4.getYamliSettingsDims(),cH=0,cG=0,cF=0,cJ=0,cE=0,cI=cz.height+2,cN=cI,cM=0,cL=0,cK=bv("insidePlacementPadding"),cD=by+2+cK;
if(bj){a4.setMainOffset(bu.left,bu.top,bu.right);switch(bv("settingsPlacement")){case"bottomRight":cH=bu.width-cz.width-1;
cG=1;cF=bu.height+1;break;case"bottomLeft":cH=1;cG=bu.width-cz.width-1;cF=bu.height+1;
break;case"topRight":cH=bu.width-cz.width-1;cG=1;cF=-cz.height-2;cJ=-bu.width+cz.width+2;
cE=-cG+2;cI=cz.height+bu.height+2;cN=cI;break;case"topLeft":cH=1;cG=bu.width-cz.width-1;
cF=-cz.height-2;cE=-cG+2;cI=cz.height+bu.height+2;cN=cI;break;case"rightTop":cH=bu.width+2;
cG=-cz.width-2;cF=2;cJ=-bu.width+2;cI=bu.height+2;cE=-cG+2;cN=bu.height+2;break;case"rightBottom":cH=bu.width+2;
cG=-cz.width-2;cF=bu.height-cz.height-1;break;case"leftTop":cH=-cz.width-2;cG=bu.width+2;
cF=2;cJ=cz.width+2;cI=bu.height+2;cE=-bu.width+2;cN=bu.height+2;break;case"leftBottom":cH=-cz.width-2;
cG=bu.width+2;cF=bu.height-cz.height-2;break;case"inside":b9.style.paddingLeft=a3+"px";
b9.style.paddingRight=bW+"px";var cA=o?cs:0;if(cm()=="rtl"){if(a3<cD){b9.style.paddingLeft=cD+"px"
}cH=cv+cA+1+cK;cG=bu.width-cz.width-cx-1-cA-cK}else{if(v){cA=cs}if(bW<cD){b9.style.paddingRight=cD+"px"
}cH=bu.width-cz.width-cx-1-cA-cK;cG=cv+1+cA+cK}cF=bV;cM=-by-2;cL=by+2;cJ=-cH;cI=-cF+bu.height+2;
cN=cI;cE=-cG;break}}else{var cQ=bu.top+bu.height;a4.setMainOffset(bu.left,cQ,bu.right);
switch(bv("settingsPlacement")){case"bottomRight":cH=bu.width-cz.width-1;cG=1;cF=2;
break;case"bottomLeft":cH=1;cG=bu.width-cz.width-1;cF=2;break;case"topRight":cH=bu.width-cz.width-1;
cG=1;cF=-bu.height-cz.height;cI=cz.height+bu.height+1;cN=cI;break;case"topLeft":cH=1;
cG=bu.width-cz.width-1;cF=-bu.height-cz.height;cI=cz.height+bu.height+1;cN=cI;break;
case"rightTop":cH=bu.width+2;cG=-cz.width-2;cF=-bu.height+1;break;case"rightCenter":cH=bu.width+2;
cG=-cz.width-2;cF=Math.floor((-bu.height-cz.height)/2)+1;break;case"rightBottom":cH=bu.width+2;
cG=-cz.width-2;cF=-cz.height+1;break;case"leftTop":cH=-cz.width-2;cG=bu.width+2;cF=-bu.height;
break;case"leftCenter":cH=-cz.width-2;cG=bu.width+2;cF=Math.floor((-bu.height-cz.height)/2)-1;
break;case"leftBottom":cH=-cz.width-2;cG=bu.width+2;cF=-cz.height-2;break;case"inside":if(!o){b9.style.paddingLeft=a3+"px";
b9.style.paddingRight=bW+"px"}if(cm()=="rtl"){if(!o&&a3<cD){b9.style.paddingLeft=cD+"px"
}cH=cv+1+cK;cG=bu.width-cz.width-cx-1-cK}else{if(!o&&bW<cD){b9.style.paddingRight=cD+"px"
}cH=bu.width-cz.width-cx-1-cK;cG=cv+1+cK}cF=-bu.height+bV+Math.floor(((bu.height-bV-ci)-by)/2)-bU-1;
cM=-by-2;cL=by+2;cJ=-cH;cI=-cF;cN=cI;cE=-cG;break}}a4.setYamliSettingsPosition(cH+bv("settingsXOffset"),cF+bv("settingsYOffset"),cG-bv("settingsXOffset"));
a4.setYamliSettingsMenuPosition(by+cM+2,cz.height+cL+2,0);a4.setYamliTipsPosition(cH+cJ,cF+cI,cG+cE,cF+cN);
if(bv("settingsPlacement")=="hide"){a4.hideYamliSettings()}else{a4.showYamliSettings()
}},bo=function(){var cz=af(b9);if(cz.start!=cz.end){return aL}return i(b9.value,cz,s,bv("disableInMarkup"))
},bZ=function(cz){if(cz==bb){return}cf(false);a7=H.createTransliterationRequest(cz);
bb=cz;a7.onreadystatechange=function(){var cA=H.processTransliterationResponse(a7);
if(cA>=0){a7=aL;if(cA==1){ba(true,false)}}};a7.send()},bA=function(cB){cf(false);
var cz={request:aL,wordSelection:cB},cA=H.createTransliterationRequest(cB.word);cz.request=cA;
cA.onreadystatechange=function(){var cC=H.processTransliterationResponse(cA);if(cC>=0){for(var cD=0;
cD<a9.length;++cD){if(a9[cD]==cz){a9.splice(cD,1)}}if(cC==1){b6(cB,s)}}};a9.push(cz);
cA.send()},bl=function(){if(cm()=="rtl"){b9.spellcheck=s}else{b9.spellcheck=az}},bw=function(cE,cB,cC){if(cB==cC){return
}var cD=cC-cB;for(var cA=0;cA<a9.length;++cA){var cz=a9[cA];if(cz.wordSelection.end<=cE){continue
}cz.wordSelection.end+=cD;cz.wordSelection.start+=cD}},cf=function(cA){if(cA){a4.hide(false)
}b7();bb=aL;if(a7!==aL){var cz=a7;a7=aL;cz.onreadystatechange=function(){H.processTransliterationResponse(cz)
}}},cc=function(cz){return a7!==aL||a9.length>0||(cz&&bp)},bg=function(cz){b4(cz);
ba(true,false)},b4=function(cF){var cE=[],cC,cA=0,cG=0;for(var cB=0;cB<cF.length;
++cB){cC=cF.charAt(cB);cE.push(aJ[cC]);if(cC==","){++cA}else{if(cC=="."){++cG}}}var cz=cE.join(""),cD=[];
cD.push({trans:cz,type:1});if(cA===1&&cG===0){cD.push({trans:cz.replace(aJ[","],aJ["."]),type:1})
}H.registerTransliteration(cF,cD,H.readGlobalPref(aD)?0:1)},a1=function(cz,cB){var cH={l:0,r:0,y:0,l2:0,r2:0};
if(!ck){var cE=document.selection.createRange(),cD=(am&&am.clientWidth===0)?g:am,cC;
if(bj){cE.move("character",-cz.selectionOffset);cE.moveEnd("character",cz.word.length);
cC=cE.getClientRects()[0];if(top.location!=location){cH.l=cE.offsetLeft-bu.left+cD.scrollLeft;
cH.y=cE.offsetTop-bu.top+cC.bottom-cC.top+g.scrollTop+am.scrollTop-cD.clientTop;cH.r2=bu.width-cH.l-(cC.right-cC.left)+cD.clientLeft
}else{cH.l=cC.left-bu.left+cD.scrollLeft-cD.clientLeft;cH.y=cC.bottom-bu.top+g.scrollTop+am.scrollTop-cD.clientTop;
cH.r2=bu.width-cH.l-(cC.right-cC.left)}if(cH.l<0||cH.y<0){return aL}}else{cE.move("character",-cz.selectionOffset);
cE.moveEnd("character",cz.word.length);cC=cE.getClientRects()[0];if(top.location!=location){cH.l=cE.offsetLeft;
cH.r2=bu.width-cH.l-(cC.right-cC.left)}else{cH.l=cC.left-bu.left+(F?cD.scrollLeft:cD.scrollLeft)-cD.clientLeft;
cH.r2=bu.width-cH.l-(cC.right-cC.left)}cH.y=0}cH.r=cH.l+cC.right-cC.left;cH.l2=cH.r2+cC.right-cC.left
}else{if(bj){var cG=bc(b9.value,cz);cH.l=cG.x;cH.y=cG.y}else{var cA;if(cz.wordType==z.I.WordType.PURE_ARABIC){cA=cz.word.substr(cz.selectionOffset)
}else{cA=cz.word.substr(0,cz.selectionOffset)}var cF=ca(cA);cH.l=cB-cF}cH.r=cH.l+ca(cz.word)
}return cH},a2=function(cz){window.open("http://api.yamli.com/nextgen/ajaxTransliterate.aspx?word="+aq(cz),"yamli_popup")
},b6=function(cD,cB){var cC=H.getTransliterationsFromWordSelection(cD);if(cC){if(!bT(cD)){return
}if(cC.getTransliterationCount()>0){var cA=cC.getSelectedTransliteration();if(cA==cD.word){return
}H.registerArabicToRoman(cA,cC.getRomanWord());H.registerDefaultPick(cB&&cC.getSelectedIndex()==1&&cD.selectionEnd==b9.value.length);
var cz=b5(cA,cD.start,cD.end,s,az);cq=cA;H.reportTransliterationSelection(cC,cz.start,b9.value,az)
}}else{switch(cD.wordType){case z.I.WordType.PURE_ROMAN:case z.I.WordType.ROMAN_ARABIC:bA(cD);
break;case z.I.WordType.PURE_NUMBER:b4(cD.word);b6(cD,s);break}}},bT=function(cA){var cz=s;
if(b9.value.length>=cA.end){cz=(cA.word==b9.value.substring(cA.start,cA.end))}return cz
},ba=function(cD,cA){cf(false);var cE=bo();if(cE===aL){a4.hide();return}if(!cD&&cE.isEdge){if(cE.word.length>1||cE.selectionOffset!==0){a4.hide();
return}}var cB=H.getTransliterationsFromWordSelection(cE);if(cB&&cB.getTransliterationCount()===0){a4.hide();
return}if(cB!==aL||cE.wordType==z.I.WordType.PURE_ROMAN||cE.wordType==z.I.WordType.ROMAN_ARABIC){var cz=a1(cE,cu.x);
if(cz===aL){a4.hide();return}var cF=ch()?cz.r:cz.l;var cC=ch()?cz.r2:cz.l2;if(ck&&!bj){cF=bN(cF)
}a4.setTransliterations(cB,cE,bv("maxResults"));a4.setMenuOffset(cF,cz.y,cC,ch());
a4.show()}else{a4.hide()}if(!cB){if(!cA){return true}switch(cE.wordType){case z.I.WordType.PURE_ROMAN:case z.I.WordType.ROMAN_ARABIC:bZ(cE.word);
break;case z.I.WordType.PURE_NUMBER:bg(cE.word);break}}},cl=function(){cf(true)},bh=function(cz){cu=cz
},cn=function(cz){var cA=cu.x+cz,cG=0;if(bt>0){var cC=af(b9);if(cA<0){var cB=b9.value.substr(0,cC.start),cE=ca(cB);
cG=(cE>bt)?bt:cE}else{if(cA>bu.width){var cF=b9.value.slice(cC.end),cD=ca(cF);cG=(cD>bt)?-bt:-cD
}}}cA+=cG;cu.x=bN(cA)},bN=function(cz){var cC=ca(b9.value);if(cC<bu.width){var cE=bo();
if(!cE){return cz}var cB=cm()=="rtl",cD=X(b9.value,cE.end,cB),cA=ca(cD);if(cB){cz=bu.width-cA-bE-cx;
if(ch()){cz+=ca(cE.word)}}else{cz=cA+br+cv;if(!ch()){cz-=ca(cE.word)}}}if(cz<0){cz=0
}else{if(cz>bu.width){cz=bu.width}}return cz},b5=function(cT,cM,cB,cI,cC){var cX=af(b9),cQ=b9.value.substr(0,cM),cA=b9.value.slice(cB),cL=b9.value.substring(cM,cB),cH=aU(cT),cK=aU(cL),cP=0,cz=0;
if(cC){var cJ=i(b9.value,{start:cM,end:cM},az,bv("disableInMarkup")),cF=H.isRegisteredDashedWord(cJ.word);
if(cH===z.I.WordType.PURE_ROMAN||cH===z.I.WordType.MIXED){if(cF){if(cQ.length>0&&!cQ.substr(cQ.length-1).match(z.I.HasPunctuationRegexp)){cQ+="-";
cP=1}if(cA.length>0&&!cA.substr(0,1).match(z.I.HasPunctuationRegexp)){cA="-"+cA;cz=1
}}}else{if(cH===z.I.WordType.PURE_ARABIC){var cN,cD;if(cQ.length>0&&(cQ.charAt(cQ.length-1)=="-"||cF)){var cG=cQ.length-1;
cD=i(cQ,{start:cG,end:cG},az,bv("disableInMarkup"));if(cD!==aL){cN=cD.word}}var cV,cS;
if(cA.length>0&&(cA.charAt(0)=="-"||cF)){var cO=1;cS=i(cA,{start:cO,end:cO},az,bv("disableInMarkup"));
if(cS!==aL){cV=cS.word}}if(cN||cV){if(cK===z.I.WordType.PURE_ROMAN||cK===z.I.WordType.MIXED){if(cD&&cD.wordType===z.I.WordType.PURE_ARABIC){cQ=cQ.substr(0,cQ.length-1);
cP=-1}if(cS&&cS.wordType===z.I.WordType.PURE_ARABIC){cA=cA.substr(1);cz=-1}}var cU={wordArray:[]};
bm(cU,cN);bm(cU,cT);bm(cU,cV);H.registerDashedWord(cU.wordArray)}}}}var cR=b9.scrollTop;
b9.value=cQ+cT+cA;b9.scrollTop=cR;var cW=cB-cM,cE=cT.length;if(cP<0){cM+=cP;cW-=cP
}else{if(cP>0){cE+=cP}}if(cz<0){cB-=cz;cW-=cz}else{if(cz>0){cE+=cz}}bw(cM,cW,cE);
if(cE!=cW){if(cX.start>=(cM+cW)){cX.start+=cE-cW}if(cX.end>=(cM+cW)){cX.end+=cE-cW
}}if(cI){cX.start=cM+cE;cX.end=cX.start}a6(cX.start,cX.end);if(bv("generateOnChangeEvent")){L(b9,"change")
}return{start:cM,end:cB}},a6=function(cE,cz){var cC=document.activeElement;if(b9.setSelectionRange){b9.setSelectionRange(cE,cz)
}else{if(b9.createTextRange){var cD=b9.value.substr(0,cE).split("\r").length-1,cB=cD+b9.value.substring(cE,cz).split("\r").length-1;
cE-=cD;cz-=cB;var cA=b9.createTextRange();cA.moveStart("character",-10000);cA.moveEnd("character",-10000);
cA.collapse();cA.moveStart("character",cE);cA.moveEnd("character",cz-cE);cA.select()
}}},bm=function(cz,cB){if(cB===aL){return}var cA=H.getDashedWordArray(cB);if(cA){cz.wordArray=cz.wordArray.concat(cA)
}else{cz.wordArray.push(cB)}},a5=function(){if(!H.getShowHint()){return}var cz=bv("hintMode");
if((cz=="startModeOff"&&!cj)||(cz=="startModeOnOrOff")){a4.showHint()}if(b9.value.length>30){a4.hideHint(false)
}},b3=function(cz){b0=az},bz=function(cz){if(a4.checkCancelBlur()||(document.activeElement&&document.activeElement.tagName=="IFRAME"&&document.activeElement.id.substr(0,5)=="yamli")){ae(cz);
return}b0=s;if(!cj){return}if(bq&&bq.yamliFocused){if(cc(true)){b9.focus();return
}if(a4.isVisible()){a4.hideAndSelectCurrent();return}}if(!o){setTimeout(bK,300)}else{bK()
}},bK=function(){if(b0){return}cl();a4.hideSettings();a4.hideHint()},bP=function(cA){H.reportTyped();
if(!cj){return}a0=az;cg=s;bR=s;H.clearMouseMoved();ce=s;bM=s;var cC=aC(cA);if(bI&&cC==13){if(cc(true)){cg=az
}else{b7();bR=az}}else{b7()}switch(cC){case 33:case 34:case 35:case 36:case 45:case 46:bR=az;
cf(true);return}if(ck){var cD=af(b9),cz,cB;if(cC==37){cz=b9.value.substr(cD.start-1,1);
cB=ca(cz);cn(-cB)}else{if(cC==39){cz=b9.value.substr(cD.end,1);cB=ca(cz);cn(cB)}}}if(cC==32&&P(cA)){bM=az
}if(a4.isVisibleAndNotLoading()){switch(cC){case 9:a4.hideAndSelectCurrent();return;
case 13:a4.hideAndSelectCurrent(true);ce=az;cg=az;return ae(cA);case 38:a4.processUpKey();
return ae(cA);case 40:a4.processDownKey();return ae(cA);case 32:if(a4.hasSelectionChanged()){var cE=bo();
if(cE.start+cE.selectionOffset<cE.end){a4.hideAndSelectCurrent();ce=az;cg=az;return ae(cA)
}}}}if(a4.isVisible()){switch(cC){case 9:a4.hide();bA(bo());return;case 27:a4.hide(true);
ce=az;return ae(cA);case 37:case 39:a4.hide(true);break}}else{if(cC==13){H.registerDefaultPick(false);
if(bj){ce=az;cf(true)}}}},aW=function(cA){if(!cj){return}if(!a0){cg=az;return}a0=s;
if(bR){return}if(cg){return ae(cA)}var cC=aC(cA),cB=150,cz=cB;bG=az;switch(cC){case 38:if(a4.isVisibleAndNotLoading()){return ae(cA)
}a4.hide();if(bj){cz=2000}break;case 40:if(a4.isVisibleAndNotLoading()){return ae(cA)
}a4.hide();if(bj){cz=2000}break;case 37:case 39:var cE=af(b9);if(cE.start!=cE.end){return
}if(bj){var cF=bo();if(cF!==aL&&cq==cF.word){}else{cz=700}}break;case 252:bR=az;return
}if(ce){return}cf(false);var cD=true;if(cz==cB){cD=ba(bG,false)}if(cD){bp=setTimeout(bY,cz)
}},bY=function(){bp=aL;ba(bG,true)},b7=function(){if(bp){clearTimeout(bp);bp=aL}},aY=function(cG){var cF=av(cG),cD=String.fromCharCode(cF);
if(cD.match(A)){a5()}if(!cj){return}if(bR){return}if(cg){return ae(cG)}var cE=af(b9),cB=i(b9.value,cE,s,bv("disableInMarkup"));
if(cD==" "){if(cE.end==b9.value.length||b9.value.substr(cE.end,1).match(aN)){if(cB!==aL&&cB.word.match(x)){cD="-"
}}}else{if(cB!==aL&&cB.selectionEnd==cB.end&&(cB.word+cD).match(f)){return}}if(!bM&&cD.match(z.I.HasPunctuationRegexp)){if(!bj&&cF==13){return
}var cH=s;cE=af(b9);var cC=cE.start;while(cC>0&&b9.value.substr(cC-1,1).match(z.I.HasPunctuationRegexp)){cC--
}if(cC>0){var cz={start:cC-1,end:cC};cB=i(b9.value,cz,s,bv("disableInMarkup"));if(cB!==aL&&cB.isEdge){cH=cB.wordType==z.I.WordType.PURE_NUMBER;
if(cB.wordType==z.I.WordType.PURE_ROMAN||cB.wordType==z.I.WordType.ROMAN_ARABIC||(cH&&aJ[cD]===aL)){b6(cB,s);
cE=af(b9)}}}var cA=ar[cD];if(cA!==aL&&!cH&&cD!="\r"){b5(cA,cE.start,cE.end,az,s);
return ae(cG)}}},cp=function(cz){if(!cj){return}if(a4.isVisible()){var cB=a4.getWordSelection(),cA=bo();
if(cA!==aL&&cB.word==cA.word&&cB.start==cA.start&&cB.end==cA.end){a4.hide(false);
return}}bd(false);if(ck){var cC=aj(cz);bh({x:cC.x-bu.left,y:cC.y-bu.top})}ba(true,true)
},a8=function(cA){if(!cj){return}var cD=aj(cA),cC=bL(),cB=bu.width-cC,cz=cD.x-bu.left;
if((cm()=="rtl"&&cz<=cB)||(cm()=="ltr"&&cz>cC)){cl()}a4.hideSettings()},bC=function(cz){var cA=e[bv("uiLanguage")][cz];
if(cA===aL){return cz}return cA},bs=function(cz){bq.yamliFocused=s},bH=function(cz){bq.yamliFocused=az
},bx=function(cz){bq.yamliFocused=az},b8=function(cz){b9.style.direction=b9.dir=cz;
b9.style.textAlign=cz=="rtl"?"right":"left";bl()},cm=function(){return b9.style.direction
},ch=function(){return b9.style.direction=="rtl"},bv=function(cz){if(co.hasOwnProperty(cz)){return co[cz]
}return H.getOption(cz)},bB=function(cz){if(cc(true)){if(!b0){b9.focus()}return ae(cz)
}if(a4.isVisible()){a4.hideAndSelectCurrent();return}};this.adjustReplaceRequests=bw;
this.validateWordSelection=bT;this.replaceInputValue=b5;this.getInputDims=function(){return bu
};this.getInputFontSize=function(){return aZ};this.setDirection=function(cz){b8(cz);
bd(true)};this.getDirection=cm;this.useRtlMenu=ch;this.toggleDir=function(){b8((cm()=="rtl")?"ltr":"rtl");
bd(true)};this.hasFocus=function(){return b0};this.setEnabled=bQ;this.getEnabled=function(){return cj
};this.resetLayout=bd;this.getMenuFontSize=function(){return cr};this.getSettingsMenuFontSize=function(){return bD
};this.getSettingsFontSize=function(){return b1};this.getCloseFontSize=function(){return bX
};this.adjustAjaxWidth=function(cz){return parseInt(cz*bf,10)};this.getSettingsButtonSize=function(){return by
};this.getSettingsButtonOffset=function(){return bU};this.getOption=bv;this.isTextArea=function(){return bj
};this.getInput=function(){return b9};this.focusInput=function(){b9.focus()};this.setSelection=a6;
this.lang=bC;this.areRequestsPending=cc;this.isMenuVisible=function(){return a4.isMenuVisible()
};this.hideTransliterations=cl;this.activateAds=function(){a4.activateAds()};this.setForm=function(cz){if(bj){return
}bI=cz;z.I.addEvent(bI,"submit",bB)};this.setSubmitButton=function(cz){bq=cz;if(bq){z.I.addEvent(bq,"blur",bs);
z.I.addEvent(bq,"focus",bH);z.I.addEvent(bq,"mousedown",bx);bq.yamliFocused=s}};this.getScrollbarWidth=cy;
this.unload=b2;bn()},N=function(aZ,aW,a1){if(window.yamliErrorHandled===az){return
}window.yamliErrorHandled=az;var aY="";try{aY=z.I.getStackTrace().join("\n");if(aY.length>1300){aY=aY.substr(0,1300)
}}catch(a0){}var aX=H.makeUrl("/report_error.ashx?msg="+aq(aZ)+"&errUrl="+aq(aW)+"&line="+aq(a1)+"&href="+aq(window.location.href)+"&st="+aq(aY)+H.getReferrerInfo("&"),"&");
H.sendOneWay(aX)};z.setupRuntimeErrorHandling=function(){if(window.yamliErrorHandled!==aL){return
}window.yamliErrorHandled=s;window.onerror=N;return s};z.isSupported=function(){aS();
return l!=D};z.sendXHR=function(aW,aZ,a0,aY){var aX=z.I.createXHR();aX.open("POST",aW);
aX.setRequestHeader("Content-type","application/x-www-form-urlencoded");aX.setRequestHeader("Content-length",aZ.length);
aX.setRequestHeader("Connection","close");aX.send(aZ);aX.onreadystatechange=function(){if(aX.readyState!=4){return
}if(a0){if(aX.status==200){a0(true,aX.responseText)}else{a0(false)}}}};z.init=function(aY,aW,aX){if(typeof aY=="string"){if(!aW){aW={}
}aW.accountId=aY}else{aW=aY}return H.setInitParams(aW,aX)};z.yamlify=function(aX,aW){if(!H.isInitialized()){return
}if(typeof aX!="string"){return}if(typeof aW!="object"&&typeof aW!="undefined"){return
}H.setupInstancesById(aX,aW,az)};z.deyamlify=function(aW){if(!H.isInitialized()){return
}if(typeof aW!="string"){return}H.removeInstanceById(aW,az)};z.yamlifyClass=function(aX,aW){if(!H.isInitialized()){return
}if(typeof aX!="string"){return}if(typeof aW!="object"&&typeof aW!="undefined"){return
}H.setupInstancesByClass(aX,aW,az)};z.deyamlifyClass=function(aW){if(!H.isInitialized()){return
}if(typeof aW!="string"){return}H.removeInstancesByClass(aW,az)};z.setGlobalOptions=function(aW){if(!H.isInitialized()){return
}if(typeof aW!="object"&&typeof aW!="undefined"){return}H.setGlobalOptions(aW,s,az)
};z.setClassOptions=function(aX,aW){if(!H.isInitialized()){return}if(typeof aX!="string"){return
}if(typeof aW!="object"&&typeof aW!="undefined"){return}H.setupInstancesByClass(aX,aW,s)
};z.setIdOptions=function(aX,aW){if(!H.isInitialized()){return}if(typeof aX!="string"){return
}if(typeof aW!="object"&&typeof aW!="undefined"){return}H.setupInstancesById(aX,aW,s)
};z.I.getStackTrace=function(){var a7;try{a7.z()}catch(a0){try{if(a0.stack){return a0.stack.replace(/^.*?\n/,"").replace(/(?:\n@:0)?\s+$/m,"").replace(/^\(/gm,"{anonymous}(").split("\n")
}else{var a8=arguments.callee.caller,a4=10,aX="function",aW="{anonymous}",a6=/function\s*([\w\-$]+)?\s*\(/i,a5=[],aY=0,a3,a1,aZ;
while(a8&&a4){--a4;a3=a6.test(a8.toString())?RegExp.$1||aW:aW;a1=a5.slice.call(a8["arguments"]);
aZ=a1.length;while(aZ--){switch(typeof a1[aZ]){case"string":a1[aZ]='"'+a1[aZ].replace(/"/g,'\\"')+'"';
break;case"function":a1[aZ]=aX;break}}a5[aY++]=a3+"("+a1.join()+")";a8=a8.caller}return a5
}}catch(a2){return[]}}return[]};z.global=H=new E()})()};
