function selectAllorRemoveAll()
{
if(document.verbform.all.checked){
$("#tenses").hide();
document.verbform.past.checked=true;
document.verbform.future.checked=true;
document.verbform.passive.checked=true;
document.verbform.imperative.checked=true;
document.verbform.confirmed.checked=true;
document.verbform.future_moode.checked=true;
}
else{
$("#tenses").show();
document.verbform.past.checked=false;
document.verbform.future.checked=false;
document.verbform.passive.checked=false;
document.verbform.imperative.checked=false;
document.verbform.confirmed.checked=false;
document.verbform.future_moode.checked=false;
}
}
// tabs and dialog

			$(function (){
							// Tabs
				$('#tabs').tabs();

				// Dialog			
				$('#dialog').dialog({
					autoOpen: false,
					width: 600,
					buttons: {
						"موافق": function() { 
							$(this).dialog("close"); 
						}, 
						"إلغاء": function() { 
							$(this).dialog("close"); 
						} 
					}
				});
				// Dialog Link
				$('#dialog_link').click(function(){
					$('#dialog').dialog('open');
					return false;
				});
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);
			});
			
			$('document').ready(function(){

/* ajust font size*/
	fontSize("#holder1", ".result", 12, 18,72);

/* show more options*/
	$("#LinkMoreOptions").click( function () {
	if ($('#BMoreOptions').text()!="[-]")
	{
	$('#BMoreOptions').text("[-]");
	$('#TextMoreOptions').text("إخفاء الخيارات");
	}
	else
	{
	$('#BMoreOptions').text("[+]");
	$('#TextMoreOptions').text("إظهار الخيارات");
	}
	$('#MoreOptions').toggle();
    });


	/* show help options*/
	$("#Bhelp").click( function () {
	$('#showhelp').toggle("slow");
	if ($('#Bhelp').text()!="[مساعدة]")
	{$('#Bhelp').text("[مساعدة]");}
	else {$('#Bhelp').text("[إخفاء المساعدة]");}	
	});
	/* show active voice */
	$("#Bindicative").click( function () {
		$('#Resultindicative').show();
		$('#Resultpassive').hide();

	  });
/* show passive tenses*/
	$("#Bpassive").click( function () {
		$('#Resultindicative').hide();
		$('#Resultpassive').show();
   });
 /* show passive tenses*/
	$("#Bdiacritics").click( function () {
		var str=$('#resultat').html();
		str=str.replace(/َ/g,"<span class='tashkeel'>ـَ</span>");
		str=str.replace(/ِ/g,"<span class='tashkeel'>ـِ</span>");
		str=str.replace(/ُ/g,"<span class='tashkeel'>ـُ</span>");
		str=str.replace(/ْ/g,"<span class='tashkeel'>ـْ</span>");
		/*var myfield=document.getElementById('harakat');
		myfield.value=str;*/
		$('#resultat').html(str);
   });
 
 
 }
 
 
 
 
 );